# ************************************************************
# Sequel Pro SQL dump
# バージョン 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: akippadevelop.ctimw6kx1b3q.ap-northeast-1.rds.amazonaws.com (MySQL 5.6.34-log)
# データベース: akippa
# 作成時刻: 2018-11-09 01:11:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `allowfromall` int(11) DEFAULT NULL,
  `agency` int(11) DEFAULT NULL,
  `partner` int(11) DEFAULT NULL,
  `statistics` int(11) DEFAULT NULL,
  `reservation` int(11) DEFAULT NULL,
  `reservation_restriction` varchar(100) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `owner_restriction` varchar(100) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `user_restriction` varchar(100) DEFAULT NULL,
  `parking` int(11) DEFAULT NULL,
  `parking_restriction` varchar(100) DEFAULT NULL,
  `keyword` int(11) DEFAULT NULL,
  `spot` int(11) DEFAULT NULL,
  `geolocation` int(11) DEFAULT NULL,
  `sales` int(11) DEFAULT NULL,
  `sales_irregular` int(11) DEFAULT NULL,
  `coupon` int(11) DEFAULT NULL,
  `couponcode` int(11) DEFAULT NULL,
  `couponcode_restriction` varchar(100) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `inquiry` int(11) DEFAULT NULL,
  `faq` int(11) DEFAULT NULL,
  `whatsnew` int(11) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  `analytics` int(11) DEFAULT NULL,
  `review` int(11) DEFAULT NULL,
  `authority` int(11) DEFAULT NULL,
  `reservationcreate` int(11) DEFAULT NULL,
  `owner_partner_manage` int(11) DEFAULT NULL,
  `request` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT '',
  `parking_management_user` int(11) NOT NULL DEFAULT '0',
  `parking_management_organization` int(11) NOT NULL DEFAULT '0',
  `parking_pin_group` int(11) NOT NULL DEFAULT '0',
  `incident_authority` int(11) NOT NULL DEFAULT '0',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_parking_daily_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_parking_daily_info`;

CREATE TABLE `analysis_parking_daily_info` (
  `date` date NOT NULL,
  `parking_id` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  KEY `idx_analysis_parking_daily_info` (`date`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_parking_space_daily
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_parking_space_daily`;

CREATE TABLE `analysis_parking_space_daily` (
  `date` date NOT NULL,
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL,
  `use_count_hourly` int(11) NOT NULL,
  `use_count_daily` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `fee_per_unit` int(11) DEFAULT NULL,
  `available_days` int(11) DEFAULT NULL,
  `is_hourly` tinyint(1) DEFAULT NULL,
  `car_number_required` tinyint(1) DEFAULT NULL,
  `is_today_unavailable` tinyint(1) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  UNIQUE KEY `date_parking_space_id` (`date`,`parking_space_id`),
  KEY `idx_pid_date_psid` (`parking_id`,`date`,`parking_space_id`),
  KEY `idx_pid` (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_parking_space_daily_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_parking_space_daily_info`;

CREATE TABLE `analysis_parking_space_daily_info` (
  `date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parking_space_id` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  KEY `idx_analysis_parking_space_daily_info` (`date`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_parking_space_sales_ltv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_parking_space_sales_ltv`;

CREATE TABLE `analysis_parking_space_sales_ltv` (
  `month` varchar(10) NOT NULL DEFAULT '',
  `parking_space_count` int(11) DEFAULT NULL,
  `n0` int(11) NOT NULL DEFAULT '0',
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL,
  `n7` int(11) DEFAULT NULL,
  `n8` int(11) DEFAULT NULL,
  `n9` int(11) DEFAULT NULL,
  `n10` int(11) DEFAULT NULL,
  `n11` int(11) DEFAULT NULL,
  `n12` int(11) DEFAULT NULL,
  `n13` int(11) DEFAULT NULL,
  `n14` int(11) DEFAULT NULL,
  `n15` int(11) DEFAULT NULL,
  `n16` int(11) DEFAULT NULL,
  `n17` int(11) DEFAULT NULL,
  `n18` int(11) DEFAULT NULL,
  `n19` int(11) DEFAULT NULL,
  `n20` int(11) DEFAULT NULL,
  `n21` int(11) DEFAULT NULL,
  `n22` int(11) DEFAULT NULL,
  `n23` int(11) DEFAULT NULL,
  `n24` int(11) DEFAULT NULL,
  `n25` int(11) DEFAULT NULL,
  `n26` int(11) DEFAULT NULL,
  `n27` int(11) DEFAULT NULL,
  `n28` int(11) DEFAULT NULL,
  `n29` int(11) DEFAULT NULL,
  `n30` int(11) DEFAULT NULL,
  `n31` int(11) DEFAULT NULL,
  `n32` int(11) DEFAULT NULL,
  `n33` int(11) DEFAULT NULL,
  `n34` int(11) DEFAULT NULL,
  `n35` int(11) DEFAULT NULL,
  `n36` int(11) DEFAULT NULL,
  PRIMARY KEY (`month`,`n0`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_parking_space_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_parking_space_status`;

CREATE TABLE `analysis_parking_space_status` (
  `month` varchar(10) NOT NULL DEFAULT '',
  `parking_space_count` int(11) DEFAULT NULL,
  `n0` int(11) NOT NULL DEFAULT '0',
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL,
  `n7` int(11) DEFAULT NULL,
  `n8` int(11) DEFAULT NULL,
  `n9` int(11) DEFAULT NULL,
  `n10` int(11) DEFAULT NULL,
  `n11` int(11) DEFAULT NULL,
  `n12` int(11) DEFAULT NULL,
  `n13` int(11) DEFAULT NULL,
  `n14` int(11) DEFAULT NULL,
  `n15` int(11) DEFAULT NULL,
  `n16` int(11) DEFAULT NULL,
  `n17` int(11) DEFAULT NULL,
  `n18` int(11) DEFAULT NULL,
  `n19` int(11) DEFAULT NULL,
  `n20` int(11) DEFAULT NULL,
  `n21` int(11) DEFAULT NULL,
  `n22` int(11) DEFAULT NULL,
  `n23` int(11) DEFAULT NULL,
  `n24` int(11) DEFAULT NULL,
  `n25` int(11) DEFAULT NULL,
  `n26` int(11) DEFAULT NULL,
  `n27` int(11) DEFAULT NULL,
  `n28` int(11) DEFAULT NULL,
  `n29` int(11) DEFAULT NULL,
  `n30` int(11) DEFAULT NULL,
  `n31` int(11) DEFAULT NULL,
  `n32` int(11) DEFAULT NULL,
  `n33` int(11) DEFAULT NULL,
  `n34` int(11) DEFAULT NULL,
  `n35` int(11) DEFAULT NULL,
  `n36` int(11) DEFAULT NULL,
  PRIMARY KEY (`month`,`n0`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_reservation_daily_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_reservation_daily_info`;

CREATE TABLE `analysis_reservation_daily_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reservation_id` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `is_price_correct` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reservation_id` (`reservation_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_search_location
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_search_location`;

CREATE TABLE `analysis_search_location` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `search_date` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` varchar(100) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `car_type` varchar(100) DEFAULT NULL,
  `use_day_from` varchar(255) DEFAULT NULL,
  `use_day_to` varchar(12) DEFAULT NULL,
  `vacancy` tinyint(1) NOT NULL DEFAULT '0',
  `device` varchar(100) DEFAULT NULL,
  `parking_count_near` int(11) NOT NULL DEFAULT '0',
  `parking_count_distant` int(11) NOT NULL DEFAULT '0',
  `parking_not_available_count_near` int(11) NOT NULL DEFAULT '0',
  `parking_not_available_count_distant` int(11) NOT NULL DEFAULT '0',
  `search_type` varchar(100) DEFAULT NULL,
  `user_lat` double DEFAULT NULL,
  `user_lng` double DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lat_lng` (`lat`,`lng`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_search_location_request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_search_location_request`;

CREATE TABLE `analysis_search_location_request` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `request` varchar(20000) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_analysis_search_location_request_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_user_return_rate1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_user_return_rate1`;

CREATE TABLE `analysis_user_return_rate1` (
  `week` date NOT NULL,
  `area` varchar(100) DEFAULT NULL,
  `user_count` int(11) DEFAULT NULL,
  `d1` int(11) DEFAULT NULL,
  `d3` int(11) DEFAULT NULL,
  `d7` int(11) DEFAULT NULL,
  `d14` int(11) DEFAULT NULL,
  `d21` int(11) DEFAULT NULL,
  `d30` int(11) DEFAULT NULL,
  `d60` int(11) DEFAULT NULL,
  `d90` int(11) DEFAULT NULL,
  `d120` int(11) DEFAULT NULL,
  `d150` int(11) DEFAULT NULL,
  `d180` int(11) DEFAULT NULL,
  `d240` int(11) DEFAULT NULL,
  `d300` int(11) DEFAULT NULL,
  `d360` int(11) DEFAULT NULL,
  `d420` int(11) DEFAULT NULL,
  `d480` int(11) DEFAULT NULL,
  `d540` int(11) DEFAULT NULL,
  `d600` int(11) DEFAULT NULL,
  `d660` int(11) DEFAULT NULL,
  `d720` int(11) DEFAULT NULL,
  `d780` int(11) DEFAULT NULL,
  `d840` int(11) DEFAULT NULL,
  `d900` int(11) DEFAULT NULL,
  `d960` int(11) DEFAULT NULL,
  `d1020` int(11) DEFAULT NULL,
  `d1080` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_user_return_rate2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_user_return_rate2`;

CREATE TABLE `analysis_user_return_rate2` (
  `week` date NOT NULL,
  `area` varchar(100) DEFAULT NULL,
  `user_count` int(11) DEFAULT NULL,
  `d30` int(11) DEFAULT NULL,
  `d60` int(11) DEFAULT NULL,
  `d90` int(11) DEFAULT NULL,
  `d120` int(11) DEFAULT NULL,
  `d150` int(11) DEFAULT NULL,
  `d180` int(11) DEFAULT NULL,
  `d210` int(11) DEFAULT NULL,
  `d240` int(11) DEFAULT NULL,
  `d270` int(11) DEFAULT NULL,
  `d300` int(11) DEFAULT NULL,
  `d330` int(11) DEFAULT NULL,
  `d360` int(11) DEFAULT NULL,
  `d390` int(11) DEFAULT NULL,
  `d420` int(11) DEFAULT NULL,
  `d450` int(11) DEFAULT NULL,
  `d480` int(11) DEFAULT NULL,
  `d510` int(11) DEFAULT NULL,
  `d540` int(11) DEFAULT NULL,
  `d570` int(11) DEFAULT NULL,
  `d600` int(11) DEFAULT NULL,
  `d630` int(11) DEFAULT NULL,
  `d660` int(11) DEFAULT NULL,
  `d690` int(11) DEFAULT NULL,
  `d720` int(11) DEFAULT NULL,
  `d750` int(11) DEFAULT NULL,
  `d780` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ analysis_user_sales_ltv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analysis_user_sales_ltv`;

CREATE TABLE `analysis_user_sales_ltv` (
  `month` varchar(10) NOT NULL DEFAULT '',
  `user_count` int(11) DEFAULT NULL,
  `n0` int(11) NOT NULL DEFAULT '0',
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `n3` int(11) DEFAULT NULL,
  `n4` int(11) DEFAULT NULL,
  `n5` int(11) DEFAULT NULL,
  `n6` int(11) DEFAULT NULL,
  `n7` int(11) DEFAULT NULL,
  `n8` int(11) DEFAULT NULL,
  `n9` int(11) DEFAULT NULL,
  `n10` int(11) DEFAULT NULL,
  `n11` int(11) DEFAULT NULL,
  `n12` int(11) DEFAULT NULL,
  `n13` int(11) DEFAULT NULL,
  `n14` int(11) DEFAULT NULL,
  `n15` int(11) DEFAULT NULL,
  `n16` int(11) DEFAULT NULL,
  `n17` int(11) DEFAULT NULL,
  `n18` int(11) DEFAULT NULL,
  `n19` int(11) DEFAULT NULL,
  `n20` int(11) DEFAULT NULL,
  `n21` int(11) DEFAULT NULL,
  `n22` int(11) DEFAULT NULL,
  `n23` int(11) DEFAULT NULL,
  `n24` int(11) DEFAULT NULL,
  `n25` int(11) DEFAULT NULL,
  `n26` int(11) DEFAULT NULL,
  `n27` int(11) DEFAULT NULL,
  `n28` int(11) DEFAULT NULL,
  `n29` int(11) DEFAULT NULL,
  `n30` int(11) DEFAULT NULL,
  `n31` int(11) DEFAULT NULL,
  `n32` int(11) DEFAULT NULL,
  `n33` int(11) DEFAULT NULL,
  `n34` int(11) DEFAULT NULL,
  `n35` int(11) DEFAULT NULL,
  `n36` int(11) DEFAULT NULL,
  PRIMARY KEY (`month`,`n0`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ api_key
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api_key`;

CREATE TABLE `api_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(32) NOT NULL DEFAULT '',
  `api_value` varchar(255) NOT NULL DEFAULT '',
  `del_flag` tinyint(1) DEFAULT '0',
  `register_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_api_key` (`api_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ app_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_user`;

CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL COMMENT 'WEBユーザーID',
  `auth_key` varchar(32) DEFAULT NULL COMMENT 'ログイン状態維持用認証キー',
  `access_token` varchar(255) NOT NULL COMMENT 'アクセストークン',
  `token_expire` int(11) NOT NULL COMMENT 'トークン有効期限',
  `password_reset_token` varchar(255) DEFAULT NULL COMMENT 'パスワードリセットトークン',
  `platform` varchar(8) NOT NULL COMMENT '使用プラットフォーム',
  `device_id` varchar(64) NOT NULL COMMENT '端末ID',
  `os_version` varchar(64) NOT NULL COMMENT '端末OSバージョン',
  `app_version` varchar(64) NOT NULL COMMENT 'アプリバージョン',
  `login_ip` varchar(255) NOT NULL COMMENT 'ログインIP',
  `last_login_at` int(11) NOT NULL COMMENT '最終ログイン日時',
  `created_at` int(11) NOT NULL COMMENT '作成日時',
  `updated_at` int(11) NOT NULL COMMENT '更新日時',
  `device_token` text COMMENT 'push通知用トークン',
  `reservation_notify` tinyint(1) DEFAULT '1' COMMENT '予約push通知設定',
  `notify_time` int(11) DEFAULT NULL COMMENT '予約push通知タイミング',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_app_user_access_token` (`access_token`),
  UNIQUE KEY `unq_app_user_device_id` (`device_id`),
  KEY `fk_app_user_user_id` (`user_id`),
  CONSTRAINT `fk_app_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ area
# ------------------------------------------------------------

DROP TABLE IF EXISTS `area`;

CREATE TABLE `area` (
  `area_id` varchar(100) NOT NULL DEFAULT '',
  `prefecture` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `summary` text NOT NULL,
  `content` longtext NOT NULL,
  `thumbnail` varchar(255) NOT NULL DEFAULT '',
  `bigram` longtext NOT NULL,
  `published_at` timestamp NOT NULL,
  `substitute_order` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_url` (`url`),
  FULLTEXT KEY `bigram` (`bigram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth`;

CREATE TABLE `auth` (
  `auth_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `login_time` datetime NOT NULL,
  `logout_time` datetime DEFAULT NULL,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ auth_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_admin`;

CREATE TABLE `auth_admin` (
  `auth_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `login_time` datetime NOT NULL,
  `logout_time` datetime DEFAULT NULL,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ banned_word
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banned_word`;

CREATE TABLE `banned_word` (
  `banned_word_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`banned_word_id`),
  UNIQUE KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ calendar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calendar`;

CREATE TABLE `calendar` (
  `date` date NOT NULL,
  `weekday` tinyint(4) NOT NULL DEFAULT '0',
  `holiday` tinyint(4) NOT NULL DEFAULT '0',
  `akippa_category` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ car_specification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_specification`;

CREATE TABLE `car_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` varchar(25) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `grade` varchar(60) DEFAULT NULL,
  `sales_date` varchar(20) DEFAULT NULL,
  `length` int(11) DEFAULT '0',
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `del_flag` tinyint(1) DEFAULT '0',
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_user` varchar(50) DEFAULT 'crawler_batch',
  PRIMARY KEY (`id`),
  KEY `idx_car` (`car_id`,`manufacturer`,`name`,`grade`,`sales_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ closing_month
# ------------------------------------------------------------

DROP TABLE IF EXISTS `closing_month`;

CREATE TABLE `closing_month` (
  `month` varchar(6) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ cooperative_company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cooperative_company`;

CREATE TABLE `cooperative_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `access_key` varchar(64) DEFAULT NULL,
  `confidential` tinyint(1) NOT NULL DEFAULT '0',
  `owner_api_permission` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cooperative_company_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ coupon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupon`;

CREATE TABLE `coupon` (
  `coupon_id` varchar(100) NOT NULL DEFAULT '',
  `parent_coupon_id` varchar(100) DEFAULT NULL,
  `coupon_code_id` varchar(100) DEFAULT NULL,
  `coupon_name` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `days` int(11) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `available_count` int(11) NOT NULL DEFAULT '1',
  `infinity` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `is_auto_apply` tinyint(1) DEFAULT '0',
  `regist_time` datetime NOT NULL,
  `use_time` datetime DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ coupon_bk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupon_bk`;

CREATE TABLE `coupon_bk` (
  `coupon_id` varchar(100) NOT NULL DEFAULT '',
  `parent_coupon_id` varchar(100) DEFAULT NULL,
  `coupon_code_id` varchar(100) DEFAULT NULL,
  `coupon_name` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `days` int(11) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `usage_count` int(11) NOT NULL DEFAULT '1',
  `infinity` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `is_auto_apply` tinyint(1) DEFAULT '0',
  `regist_time` datetime NOT NULL,
  `use_time` datetime DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ coupon_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupon_code`;

CREATE TABLE `coupon_code` (
  `coupon_code_id` varchar(100) NOT NULL DEFAULT '',
  `coupon_name` varchar(100) NOT NULL DEFAULT '',
  `discount_type` tinyint(4) NOT NULL DEFAULT '1',
  `rate` int(11) NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `rental_type` tinyint(4) NOT NULL DEFAULT '1',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `parking_ids` text,
  `available_days` text,
  `available_weekdays` varchar(20) DEFAULT NULL,
  `available_holiday` tinyint(4) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT '',
  `published_num` int(11) NOT NULL DEFAULT '0',
  `publish_limit_num` int(11) DEFAULT NULL,
  `usage_count` int(11) NOT NULL DEFAULT '1',
  `usable_days` int(11) NOT NULL DEFAULT '1',
  `publish_close_date` date DEFAULT NULL,
  `expire_day` smallint(5) unsigned DEFAULT '0',
  `expire_date` datetime DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `access_key` varchar(100) DEFAULT NULL,
  `user_permission` tinyint(1) DEFAULT NULL,
  `update_user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`coupon_code_id`),
  KEY `idx_access_key` (`access_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `coupon_code_insert` AFTER INSERT ON `coupon_code` FOR EACH ROW BEGIN
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'coupon_name',
        new_data = NEW.coupon_name,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
     
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'type',
        new_data = NEW.type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'status',
        new_data = NEW.status,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'usage_count',
        new_data = NEW.usage_count,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'publish_limit_num',
        new_data = NEW.publish_limit_num,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'usable_days',
        new_data = NEW.usable_days,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'expire_day',
        new_data = NEW.expire_day,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
 
    INSERT INTO update_history
    SET
        uniq_id = NEW.coupon_code_id,
        table_name = 'coupon_code',
        column_name = 'expire_date',
        new_data = NEW.expire_date,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `coupon_code_update` AFTER UPDATE ON `coupon_code` FOR EACH ROW BEGIN
    IF
        (OLD.coupon_name IS NULL AND NEW.coupon_name IS NOT NULL) OR 
        (OLD.coupon_name IS NOT NULL AND NEW.coupon_name IS NULL) OR 
        NEW.coupon_name != OLD.coupon_name
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'coupon_name',
            old_data = OLD.coupon_name,
            new_data = NEW.coupon_name,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
     
    IF
        (OLD.type IS NULL AND NEW.type IS NOT NULL) OR 
        (OLD.type IS NOT NULL AND NEW.type IS NULL) OR 
        NEW.type != OLD.type
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'type',
            old_data = OLD.type,
            new_data = NEW.type,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
 
    IF
        (OLD.status IS NULL AND NEW.status IS NOT NULL) OR 
        (OLD.status IS NOT NULL AND NEW.status IS NULL) OR 
        NEW.status != OLD.status
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'status',
            old_data = OLD.status,
            new_data = NEW.status,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
 
    IF
        (OLD.usage_count IS NULL AND NEW.usage_count IS NOT NULL) OR 
        (OLD.usage_count IS NOT NULL AND NEW.usage_count IS NULL) OR 
        NEW.usage_count != OLD.usage_count
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'usage_count',
            old_data = OLD.usage_count,
            new_data = NEW.usage_count,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
     
    IF
        (OLD.publish_limit_num IS NULL AND NEW.publish_limit_num IS NOT NULL) OR 
        (OLD.publish_limit_num IS NOT NULL AND NEW.publish_limit_num IS NULL) OR 
        NEW.publish_limit_num != OLD.publish_limit_num
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'publish_limit_num',
            old_data = OLD.publish_limit_num,
            new_data = NEW.publish_limit_num,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
 
    IF
        (OLD.usable_days IS NULL AND NEW.usable_days IS NOT NULL) OR 
        (OLD.usable_days IS NOT NULL AND NEW.usable_days IS NULL) OR 
        NEW.usable_days != OLD.usable_days
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'usable_days',
            old_data = OLD.usable_days,
            new_data = NEW.usable_days,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
 
    IF
        (OLD.expire_day IS NULL AND NEW.expire_day IS NOT NULL) OR 
        (OLD.expire_day IS NOT NULL AND NEW.expire_day IS NULL) OR 
        NEW.expire_day != OLD.expire_day
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'expire_day',
            old_data = OLD.expire_day,
            new_data = NEW.expire_day,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
 
    IF
        (OLD.expire_date IS NULL AND NEW.expire_date IS NOT NULL) OR 
        (OLD.expire_date IS NOT NULL AND NEW.expire_date IS NULL) OR 
        NEW.expire_date != OLD.expire_date
    THEN
        INSERT INTO update_history
        SET
            uniq_id = NEW.coupon_code_id,
            table_name = 'coupon_code',
            column_name = 'expire_date',
            old_data = OLD.expire_date,
            new_data = NEW.expire_date,
            update_user_id = NEW.update_user_id,
            created_at = NOW();
    END IF;
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ coupon_code_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupon_code_user`;

CREATE TABLE `coupon_code_user` (
  `coupon_code_user_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`,`coupon_code_user_id`),
  UNIQUE KEY `coupon_code_user_id` (`coupon_code_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ daily_fluctuation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `daily_fluctuation`;

CREATE TABLE `daily_fluctuation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type` tinyint(1) NOT NULL,
  `prefecture` varchar(20) NOT NULL,
  `area` tinyint(1) DEFAULT NULL,
  `contract` tinyint(1) DEFAULT NULL,
  `positive` tinyint(1) NOT NULL,
  `target_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`,`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ daily_fluctuation_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `daily_fluctuation_total`;

CREATE TABLE `daily_fluctuation_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type` tinyint(1) NOT NULL,
  `prefecture` varchar(20) NOT NULL,
  `area` tinyint(1) DEFAULT NULL,
  `contract` tinyint(1) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`,`type`,`prefecture`,`area`,`contract`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ daily_reservation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `daily_reservation`;

CREATE TABLE `daily_reservation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `used_at` date NOT NULL,
  `reserved_at` date NOT NULL,
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `prefecture` varchar(20) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL,
  `price_owner` int(11) NOT NULL,
  `pay_rate` int(11) NOT NULL,
  `rental_type` varchar(20) NOT NULL DEFAULT '',
  `area` tinyint(1) DEFAULT NULL,
  `contract` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reservation_id` (`reservation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ driver_enquete
# ------------------------------------------------------------

DROP TABLE IF EXISTS `driver_enquete`;

CREATE TABLE `driver_enquete` (
  `driver_enquete_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `q1` varchar(20) DEFAULT '',
  `q2` text,
  `q3` text,
  `purpose` varchar(100) DEFAULT NULL,
  `purpose_free` text,
  `rating_location` tinyint(1) DEFAULT NULL,
  `rating_space` tinyint(1) DEFAULT NULL,
  `rating_price` tinyint(1) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `marked` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`driver_enquete_id`),
  KEY `idx_reservation_id` (`reservation_id`),
  KEY `idx_question` (`q1`,`status`,`deleted`,`marked`),
  KEY `idx_marked` (`marked`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ f_auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `f_auth`;

CREATE TABLE `f_auth` (
  `f_auth_id` varchar(100) NOT NULL DEFAULT '',
  `facebook_user_id` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`f_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ faq_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_category`;

CREATE TABLE `faq_category` (
  `category_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `order` tinyint(4) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ faq_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_contents`;

CREATE TABLE `faq_contents` (
  `contents_id` varchar(100) NOT NULL DEFAULT '',
  `sub_category_id` varchar(100) NOT NULL DEFAULT '',
  `status` varchar(20) DEFAULT NULL,
  `show_web` tinyint(4) DEFAULT NULL,
  `show_app` tinyint(4) DEFAULT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`contents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ faq_contents_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_contents_tag`;

CREATE TABLE `faq_contents_tag` (
  `contents_id` varchar(100) NOT NULL DEFAULT '',
  `tag_id` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ faq_sub_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_sub_category`;

CREATE TABLE `faq_sub_category` (
  `sub_category_id` varchar(100) NOT NULL DEFAULT '',
  `category_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `order` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sub_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ faq_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_tag`;

CREATE TABLE `faq_tag` (
  `tag_id` varchar(100) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `order` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ favorite_parking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `favorite_parking`;

CREATE TABLE `favorite_parking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL COMMENT 'WEBユーザーID',
  `app_user_id` int(11) DEFAULT NULL COMMENT 'アプリユーザーID',
  `parking_id` varchar(255) NOT NULL COMMENT 'お気に入り駐車場ID',
  `created_at` int(11) NOT NULL COMMENT '作成日時',
  PRIMARY KEY (`id`),
  KEY `idx_favorite_parking_user_id` (`user_id`),
  KEY `idx_favorite_parking_app_user_id` (`app_user_id`),
  KEY `fk_favorite_parking_parking_id` (`parking_id`),
  CONSTRAINT `fk_favorite_parking_app_user_id` FOREIGN KEY (`app_user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `fk_favorite_parking_parking_id` FOREIGN KEY (`parking_id`) REFERENCES `parking` (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ g_auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `g_auth`;

CREATE TABLE `g_auth` (
  `g_auth_id` varchar(100) NOT NULL DEFAULT '',
  `google_user_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) DEFAULT '',
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`g_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ geo_parkings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geo_parkings`;

CREATE TABLE `geo_parkings` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `latlon` geometry NOT NULL,
  PRIMARY KEY (`parking_id`),
  SPATIAL KEY `latlon` (`latlon`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# テーブルのダンプ geolocation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geolocation`;

CREATE TABLE `geolocation` (
  `address` varchar(200) NOT NULL DEFAULT '',
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `prefecture` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `expired_at` datetime DEFAULT CURRENT_TIMESTAMP,
  KEY `idx_address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ holiday
# ------------------------------------------------------------

DROP TABLE IF EXISTS `holiday`;

CREATE TABLE `holiday` (
  `date` date NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incident
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incident`;

CREATE TABLE `incident` (
  `incident_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user_id` int(11) NOT NULL,
  `occurrence_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `large_category` varchar(100) NOT NULL,
  `medium_category` varchar(100) NOT NULL,
  `small_category` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) DEFAULT NULL,
  `parking_id` varchar(100) DEFAULT NULL,
  `remarks` text,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user_id` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`incident_id`),
  KEY `idx_occurrence_datetime` (`occurrence_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incident_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incident_history`;

CREATE TABLE `incident_history` (
  `incident_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `incident_id` int(11) unsigned NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `correspondence_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `declaration_type` varchar(100) NOT NULL,
  `declaration_person` varchar(200) NOT NULL,
  `declaration_remarks` text NOT NULL,
  `contact_route` varchar(100) NOT NULL,
  `contact_route_direction` varchar(100) NOT NULL,
  `remarks` text,
  `next_action` int(11) DEFAULT NULL,
  `next_action_department` varchar(100) DEFAULT '',
  `next_action_complete` int(11) DEFAULT NULL,
  `next_action_date` date DEFAULT NULL,
  `next_action_content_category` int(11) DEFAULT NULL,
  `next_action_content` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user_id` varchar(100) NOT NULL,
  PRIMARY KEY (`incident_history_id`),
  KEY `idx_correspondence_datetime` (`correspondence_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ individual_pins_for_analyticsmap
# ------------------------------------------------------------

DROP TABLE IF EXISTS `individual_pins_for_analyticsmap`;

CREATE TABLE `individual_pins_for_analyticsmap` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `map_type` varchar(20) NOT NULL DEFAULT 'colormap',
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ inquiry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inquiry`;

CREATE TABLE `inquiry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `regist_time` datetime NOT NULL,
  `reply_user` varchar(100) DEFAULT NULL,
  `reply_comment` text,
  `reply_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ inviter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inviter`;

CREATE TABLE `inviter` (
  `inviter_id` varchar(100) NOT NULL DEFAULT '',
  `invite_id` varchar(100) NOT NULL DEFAULT '',
  `campaign_id` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`inviter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ leave_enquete
# ------------------------------------------------------------

DROP TABLE IF EXISTS `leave_enquete`;

CREATE TABLE `leave_enquete` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `a1` int(11) DEFAULT NULL,
  `a2` int(11) DEFAULT NULL,
  `b1` int(11) DEFAULT NULL,
  `b2` int(11) DEFAULT NULL,
  `b3` int(11) DEFAULT NULL,
  `b4` int(11) DEFAULT NULL,
  `b5` int(11) DEFAULT NULL,
  `c1` int(11) DEFAULT NULL,
  `c2` int(11) DEFAULT NULL,
  `c3` int(11) DEFAULT NULL,
  `d1` int(11) DEFAULT NULL,
  `d2` int(11) DEFAULT NULL,
  `d3` int(11) DEFAULT NULL,
  `e1` int(11) DEFAULT NULL,
  `e2` int(11) DEFAULT NULL,
  `e3` int(11) DEFAULT NULL,
  `e4` int(11) DEFAULT NULL,
  `g1` int(11) DEFAULT NULL,
  `g2` int(11) DEFAULT NULL,
  `g3` int(11) DEFAULT NULL,
  `g4` int(11) DEFAULT NULL,
  `g5` int(11) DEFAULT NULL,
  `g6` int(11) DEFAULT NULL,
  `g6d` text,
  `g7` int(11) DEFAULT NULL,
  `g7d` text,
  `g8` int(11) DEFAULT NULL,
  `g8d` text,
  `g9` int(11) DEFAULT NULL,
  `g9d` text,
  `g10` int(11) DEFAULT NULL,
  `g10d` text,
  `g11` int(11) DEFAULT NULL,
  `g12` int(11) DEFAULT NULL,
  `g12d` text,
  `free1` text,
  `free2` text,
  `free3` text,
  `free4` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ maillog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `maillog`;

CREATE TABLE `maillog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(1000) DEFAULT NULL,
  `message` varchar(2000) DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ monthly
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monthly`;

CREATE TABLE `monthly` (
  `monthly_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `is_company` tinyint(1) DEFAULT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `phone_number` varchar(20) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`monthly_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ notification_of_available_parking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_of_available_parking`;

CREATE TABLE `notification_of_available_parking` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`parking_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ notification_of_canceled_reservation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_of_canceled_reservation`;

CREATE TABLE `notification_of_canceled_reservation` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `status` varchar(40) NOT NULL,
  `regist_time` datetime NOT NULL,
  `notification_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`parking_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ notification_of_searchparking_request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_of_searchparking_request`;

CREATE TABLE `notification_of_searchparking_request` (
  `request_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `days` varchar(2000) DEFAULT NULL,
  `vehicle_type` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(100) NOT NULL DEFAULT '',
  `purpose` varchar(10) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`request_id`),
  UNIQUE KEY `email` (`email`,`lat`,`lng`,`vehicle_type`,`keyword`),
  KEY `idx_lat_lng` (`lat`,`lng`),
  KEY `idx_status` (`status`),
  KEY `idx_regist_time` (`regist_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ notify_destination
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notify_destination`;

CREATE TABLE `notify_destination` (
  `user_id` varchar(100) NOT NULL,
  `send_type` tinyint(1) NOT NULL DEFAULT '1',
  `to` varchar(200) NOT NULL DEFAULT '',
  `notification_flag_to_owner` bigint(20) unsigned NOT NULL DEFAULT '271',
  `regist_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `author` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`,`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ old_parking_available_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `old_parking_available_info`;

CREATE TABLE `old_parking_available_info` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `price` int(11) NOT NULL,
  `available` int(11) DEFAULT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`parking_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ old_spot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `old_spot`;

CREATE TABLE `old_spot` (
  `spot_id` varchar(100) NOT NULL DEFAULT '',
  `spot_genre_id` varchar(100) NOT NULL DEFAULT '',
  `spot_key` varchar(31) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `second_description` text,
  `is_noindex` tinyint(1) DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  PRIMARY KEY (`spot_id`),
  UNIQUE KEY `idx_spot_key` (`spot_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `path` varchar(1000) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '1',
  `remarks` text NOT NULL,
  `master_owner_user_id` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ organization_payment_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organization_payment_details`;

CREATE TABLE `organization_payment_details` (
  `organization_id` int(11) unsigned NOT NULL,
  `month` int(10) unsigned NOT NULL,
  `filename` varchar(100) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`organization_id`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ owner_inquiry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `owner_inquiry`;

CREATE TABLE `owner_inquiry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT '',
  `user_id` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(200) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `address` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(100) DEFAULT NULL,
  `comment` text,
  `contact_comment` text,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ owner_invite_campaign
# ------------------------------------------------------------

DROP TABLE IF EXISTS `owner_invite_campaign`;

CREATE TABLE `owner_invite_campaign` (
  `campaign_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `prize_amount` int(11) DEFAULT NULL,
  `prize_max_count` int(11) DEFAULT NULL,
  `special_pay_rate` int(11) DEFAULT NULL,
  `special_pay_rate_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking`;

CREATE TABLE `parking` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `parking_no` int(11) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(200) DEFAULT NULL,
  `pr_text` varchar(100) NOT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `prefecture` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `image1` varchar(200) DEFAULT '',
  `caption1` text,
  `image2` varchar(200) DEFAULT '',
  `caption2` text,
  `price` int(11) DEFAULT NULL,
  `min_price` int(11) DEFAULT NULL,
  `max_price` int(11) DEFAULT NULL,
  `price_owner` int(11) DEFAULT NULL,
  `pay_rate` int(11) DEFAULT NULL,
  `start_time` varchar(10) DEFAULT NULL,
  `end_time` varchar(10) DEFAULT NULL,
  `days` varchar(2000) DEFAULT NULL,
  `days_type` enum('everyday','weekly','calendar') DEFAULT NULL,
  `days_of_week` char(13) DEFAULT NULL,
  `unavailable_days` varchar(3000) DEFAULT NULL,
  `exclude_holiday` int(11) DEFAULT NULL,
  `holiday_availability` tinyint(4) NOT NULL DEFAULT '1',
  `daily` int(11) DEFAULT '1',
  `reservation_one_day_only` tinyint(4) NOT NULL DEFAULT '0',
  `monthly` int(11) DEFAULT NULL,
  `owner_regist` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `pin_group_id` int(11) DEFAULT NULL,
  `ad` tinyint(4) NOT NULL DEFAULT '0',
  `ad_min_price` int(11) DEFAULT '0',
  `ad_min_price_info` varchar(20) DEFAULT NULL,
  `ad_price_info` varchar(1000) DEFAULT NULL,
  `ad_special_price_info` varchar(1000) DEFAULT NULL,
  `confirm_info` text,
  `note` varchar(1000) DEFAULT NULL,
  `area_info` varchar(1000) DEFAULT NULL,
  `owner_comment` varchar(1000) DEFAULT NULL,
  `staff_comment` text,
  `is_consigned` tinyint(1) NOT NULL DEFAULT '1' COMMENT '料金変更設定・1:おまかせ',
  `is_owner_confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `is_exclude_sales` tinyint(4) NOT NULL DEFAULT '0',
  `update_user_id` varchar(100) DEFAULT NULL,
  `status` varchar(40) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `photo_upload_time` datetime DEFAULT NULL,
  `schedule_update_time` datetime DEFAULT NULL,
  `settings_update_time` datetime DEFAULT NULL,
  `activate_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `is_hourly` tinyint(1) NOT NULL DEFAULT '0',
  `fee_per_unit` int(10) unsigned NOT NULL,
  `maximum_price_type` varchar(20) NOT NULL DEFAULT 'per_day',
  `can_go_inout` int(1) NOT NULL DEFAULT '1',
  `area_type` tinyint(1) DEFAULT NULL,
  `contract_type` tinyint(1) DEFAULT NULL,
  `via_partner` tinyint(4) NOT NULL DEFAULT '0',
  `partner_name` varchar(100) DEFAULT NULL,
  `car_number_required` tinyint(1) DEFAULT NULL,
  `is_today_unavailable` tinyint(1) DEFAULT NULL,
  `sharegate_type` tinyint(4) DEFAULT '0',
  `rating_count` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `rating_stars` tinyint(4) DEFAULT NULL,
  `rating_location` float DEFAULT NULL,
  `rating_space` float DEFAULT NULL,
  `rating_price` float DEFAULT NULL,
  `rating_update_time` datetime DEFAULT NULL,
  `available_days` int(11) NOT NULL DEFAULT '30',
  `new_available_days` int(11) NOT NULL DEFAULT '30',
  `external_parking_id` varchar(100) DEFAULT NULL,
  `introduction_code` varchar(40) DEFAULT NULL,
  `cooperative_company_name` varchar(100) DEFAULT NULL,
  `is_first_reservation_notification` tinyint(1) NOT NULL DEFAULT '0',
  `achievement_fixed` tinyint(4) DEFAULT NULL,
  `achievement_fixed_space_count` int(11) DEFAULT NULL,
  `achievement_fixed_expiration_month` date DEFAULT NULL,
  PRIMARY KEY (`parking_id`),
  UNIQUE KEY `uk_parkingno` (`parking_no`),
  UNIQUE KEY `uk_cooperative_key` (`cooperative_company_name`,`external_parking_id`),
  KEY `idx_userid_status` (`user_id`,`status`),
  KEY `idx_daily_status` (`daily`,`status`),
  KEY `idx_prefecture_city` (`prefecture`,`city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_parking` AFTER UPDATE ON `parking` FOR EACH ROW BEGIN
    IF (OLD.parking_id IS NULL AND NEW.parking_id IS NOT NULL) OR
       (OLD.parking_id IS NOT NULL AND NEW.parking_id IS NULL) OR
       NEW.parking_id != OLD.parking_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'parking_id',
        old_data = OLD.parking_id,
        new_data = NEW.parking_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.parking_no IS NULL AND NEW.parking_no IS NOT NULL) OR
       (OLD.parking_no IS NOT NULL AND NEW.parking_no IS NULL) OR
       NEW.parking_no != OLD.parking_no THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'parking_no',
        old_data = OLD.parking_no,
        new_data = NEW.parking_no,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.user_id IS NULL AND NEW.user_id IS NOT NULL) OR
       (OLD.user_id IS NOT NULL AND NEW.user_id IS NULL) OR
       NEW.user_id != OLD.user_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'user_id',
        old_data = OLD.user_id,
        new_data = NEW.user_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.name IS NULL AND NEW.name IS NOT NULL) OR
       (OLD.name IS NOT NULL AND NEW.name IS NULL) OR
       NEW.name != OLD.name THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'name',
        old_data = OLD.name,
        new_data = NEW.name,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.zip IS NULL AND NEW.zip IS NOT NULL) OR
       (OLD.zip IS NOT NULL AND NEW.zip IS NULL) OR
       NEW.zip != OLD.zip THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'zip',
        old_data = OLD.zip,
        new_data = NEW.zip,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.address IS NULL AND NEW.address IS NOT NULL) OR
       (OLD.address IS NOT NULL AND NEW.address IS NULL) OR
       NEW.address != OLD.address THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'address',
        old_data = OLD.address,
        new_data = NEW.address,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.prefecture IS NULL AND NEW.prefecture IS NOT NULL) OR
       (OLD.prefecture IS NOT NULL AND NEW.prefecture IS NULL) OR
       NEW.prefecture != OLD.prefecture THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'prefecture',
        old_data = OLD.prefecture,
        new_data = NEW.prefecture,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.city IS NULL AND NEW.city IS NOT NULL) OR
       (OLD.city IS NOT NULL AND NEW.city IS NULL) OR
       NEW.city != OLD.city THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'city',
        old_data = OLD.city,
        new_data = NEW.city,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.postal_code IS NULL AND NEW.postal_code IS NOT NULL) OR
       (OLD.postal_code IS NOT NULL AND NEW.postal_code IS NULL) OR
       NEW.postal_code != OLD.postal_code THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'postal_code',
        old_data = OLD.postal_code,
        new_data = NEW.postal_code,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.lat IS NULL AND NEW.lat IS NOT NULL) OR
       (OLD.lat IS NOT NULL AND NEW.lat IS NULL) OR
       NEW.lat != OLD.lat THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'lat',
        old_data = OLD.lat,
        new_data = NEW.lat,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.lng IS NULL AND NEW.lng IS NOT NULL) OR
       (OLD.lng IS NOT NULL AND NEW.lng IS NULL) OR
       NEW.lng != OLD.lng THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'lng',
        old_data = OLD.lng,
        new_data = NEW.lng,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.image1 IS NULL AND NEW.image1 IS NOT NULL) OR
       (OLD.image1 IS NOT NULL AND NEW.image1 IS NULL) OR
       NEW.image1 != OLD.image1 THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'image1',
        old_data = OLD.image1,
        new_data = NEW.image1,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.caption1 IS NULL AND NEW.caption1 IS NOT NULL) OR
       (OLD.caption1 IS NOT NULL AND NEW.caption1 IS NULL) OR
       NEW.caption1 != OLD.caption1 THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'caption1',
        old_data = OLD.caption1,
        new_data = NEW.caption1,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.image2 IS NULL AND NEW.image2 IS NOT NULL) OR
       (OLD.image2 IS NOT NULL AND NEW.image2 IS NULL) OR
       NEW.image2 != OLD.image2 THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'image2',
        old_data = OLD.image2,
        new_data = NEW.image2,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.caption2 IS NULL AND NEW.caption2 IS NOT NULL) OR
       (OLD.caption2 IS NOT NULL AND NEW.caption2 IS NULL) OR
       NEW.caption2 != OLD.caption2 THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'caption2',
        old_data = OLD.caption2,
        new_data = NEW.caption2,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.price IS NULL AND NEW.price IS NOT NULL) OR
       (OLD.price IS NOT NULL AND NEW.price IS NULL) OR
       NEW.price != OLD.price THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'price',
        old_data = OLD.price,
        new_data = NEW.price,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.min_price IS NULL AND NEW.min_price IS NOT NULL) OR
       (OLD.min_price IS NOT NULL AND NEW.min_price IS NULL) OR
       NEW.min_price != OLD.min_price THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'min_price',
        old_data = OLD.min_price,
        new_data = NEW.min_price,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.max_price IS NULL AND NEW.max_price IS NOT NULL) OR
       (OLD.max_price IS NOT NULL AND NEW.max_price IS NULL) OR
       NEW.max_price != OLD.max_price THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'max_price',
        old_data = OLD.max_price,
        new_data = NEW.max_price,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.price_owner IS NULL AND NEW.price_owner IS NOT NULL) OR
       (OLD.price_owner IS NOT NULL AND NEW.price_owner IS NULL) OR
       NEW.price_owner != OLD.price_owner THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'price_owner',
        old_data = OLD.price_owner,
        new_data = NEW.price_owner,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.pay_rate IS NULL AND NEW.pay_rate IS NOT NULL) OR
       (OLD.pay_rate IS NOT NULL AND NEW.pay_rate IS NULL) OR
       NEW.pay_rate != OLD.pay_rate THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'pay_rate',
        old_data = OLD.pay_rate,
        new_data = NEW.pay_rate,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.start_time IS NULL AND NEW.start_time IS NOT NULL) OR
       (OLD.start_time IS NOT NULL AND NEW.start_time IS NULL) OR
       NEW.start_time != OLD.start_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'start_time',
        old_data = OLD.start_time,
        new_data = NEW.start_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.end_time IS NULL AND NEW.end_time IS NOT NULL) OR
       (OLD.end_time IS NOT NULL AND NEW.end_time IS NULL) OR
       NEW.end_time != OLD.end_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'end_time',
        old_data = OLD.end_time,
        new_data = NEW.end_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.days IS NULL AND NEW.days IS NOT NULL) OR
       (OLD.days IS NOT NULL AND NEW.days IS NULL) OR
       NEW.days != OLD.days THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'days',
        old_data = OLD.days,
        new_data = NEW.days,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.days_type IS NULL AND NEW.days_type IS NOT NULL) OR
       (OLD.days_type IS NOT NULL AND NEW.days_type IS NULL) OR
       NEW.days_type != OLD.days_type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'days_type',
        old_data = OLD.days_type,
        new_data = NEW.days_type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.days_of_week IS NULL AND NEW.days_of_week IS NOT NULL) OR
       (OLD.days_of_week IS NOT NULL AND NEW.days_of_week IS NULL) OR
       NEW.days_of_week != OLD.days_of_week THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'days_of_week',
        old_data = OLD.days_of_week,
        new_data = NEW.days_of_week,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.unavailable_days IS NULL AND NEW.unavailable_days IS NOT NULL) OR
       (OLD.unavailable_days IS NOT NULL AND NEW.unavailable_days IS NULL) OR
       NEW.unavailable_days != OLD.unavailable_days THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'unavailable_days',
        old_data = OLD.unavailable_days,
        new_data = NEW.unavailable_days,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.exclude_holiday IS NULL AND NEW.exclude_holiday IS NOT NULL) OR
       (OLD.exclude_holiday IS NOT NULL AND NEW.exclude_holiday IS NULL) OR
       NEW.exclude_holiday != OLD.exclude_holiday THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'exclude_holiday',
        old_data = OLD.exclude_holiday,
        new_data = NEW.exclude_holiday,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.holiday_availability IS NULL AND NEW.holiday_availability IS NOT NULL) OR
       (OLD.holiday_availability IS NOT NULL AND NEW.holiday_availability IS NULL) OR
       NEW.holiday_availability != OLD.holiday_availability THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'holiday_availability',
        old_data = OLD.holiday_availability,
        new_data = NEW.holiday_availability,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.daily IS NULL AND NEW.daily IS NOT NULL) OR
       (OLD.daily IS NOT NULL AND NEW.daily IS NULL) OR
       NEW.daily != OLD.daily THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'daily',
        old_data = OLD.daily,
        new_data = NEW.daily,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.monthly IS NULL AND NEW.monthly IS NOT NULL) OR
       (OLD.monthly IS NOT NULL AND NEW.monthly IS NULL) OR
       NEW.monthly != OLD.monthly THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'monthly',
        old_data = OLD.monthly,
        new_data = NEW.monthly,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.owner_regist IS NULL AND NEW.owner_regist IS NOT NULL) OR
       (OLD.owner_regist IS NOT NULL AND NEW.owner_regist IS NULL) OR
       NEW.owner_regist != OLD.owner_regist THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'owner_regist',
        old_data = OLD.owner_regist,
        new_data = NEW.owner_regist,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.type IS NULL AND NEW.type IS NOT NULL) OR
       (OLD.type IS NOT NULL AND NEW.type IS NULL) OR
       NEW.type != OLD.type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'type',
        old_data = OLD.type,
        new_data = NEW.type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;

    IF (OLD.ad IS NULL AND NEW.ad IS NOT NULL) OR
       (OLD.ad IS NOT NULL AND NEW.ad IS NULL) OR
       NEW.ad != OLD.ad THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'ad',
        old_data = OLD.ad,
        new_data = NEW.ad,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;

    IF (OLD.ad_min_price IS NULL AND NEW.ad_min_price IS NOT NULL) OR
       (OLD.ad_min_price IS NOT NULL AND NEW.ad_min_price IS NULL) OR
       NEW.ad_min_price != OLD.ad_min_price THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'ad_min_price',
        old_data = OLD.ad_min_price,
        new_data = NEW.ad_min_price,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;

    IF (OLD.ad_min_price_info IS NULL AND NEW.ad_min_price_info IS NOT NULL) OR
       (OLD.ad_min_price_info IS NOT NULL AND NEW.ad_min_price_info IS NULL) OR
       NEW.ad_min_price_info != OLD.ad_min_price_info THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'ad_min_price_info',
        old_data = OLD.ad_min_price_info,
        new_data = NEW.ad_min_price_info,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;

    IF (OLD.ad_price_info IS NULL AND NEW.ad_price_info IS NOT NULL) OR
       (OLD.ad_price_info IS NOT NULL AND NEW.ad_price_info IS NULL) OR
       NEW.ad_price_info != OLD.ad_price_info THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'ad_price_info',
        old_data = OLD.ad_price_info,
        new_data = NEW.ad_price_info,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;

    IF (OLD.ad_special_price_info IS NULL AND NEW.ad_special_price_info IS NOT NULL) OR
       (OLD.ad_special_price_info IS NOT NULL AND NEW.ad_special_price_info IS NULL) OR
       NEW.ad_special_price_info != OLD.ad_special_price_info THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'ad_special_price_info',
        old_data = OLD.ad_special_price_info,
        new_data = NEW.ad_special_price_info,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.note IS NULL AND NEW.note IS NOT NULL) OR
       (OLD.note IS NOT NULL AND NEW.note IS NULL) OR
       NEW.note != OLD.note THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'note',
        old_data = OLD.note,
        new_data = NEW.note,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.area_info IS NULL AND NEW.area_info IS NOT NULL) OR
       (OLD.area_info IS NOT NULL AND NEW.area_info IS NULL) OR
       NEW.area_info != OLD.area_info THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'area_info',
        old_data = OLD.area_info,
        new_data = NEW.area_info,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.owner_comment IS NULL AND NEW.owner_comment IS NOT NULL) OR
       (OLD.owner_comment IS NOT NULL AND NEW.owner_comment IS NULL) OR
       NEW.owner_comment != OLD.owner_comment THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'owner_comment',
        old_data = OLD.owner_comment,
        new_data = NEW.owner_comment,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.staff_comment IS NULL AND NEW.staff_comment IS NOT NULL) OR
       (OLD.staff_comment IS NOT NULL AND NEW.staff_comment IS NULL) OR
       NEW.staff_comment != OLD.staff_comment THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'staff_comment',
        old_data = OLD.staff_comment,
        new_data = NEW.staff_comment,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.is_consigned IS NULL AND NEW.is_consigned IS NOT NULL) OR
       (OLD.is_consigned IS NOT NULL AND NEW.is_consigned IS NULL) OR
       NEW.is_consigned != OLD.is_consigned THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'is_consigned',
        old_data = OLD.is_consigned,
        new_data = NEW.is_consigned,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR
       (OLD.status IS NOT NULL AND NEW.status IS NULL) OR
       NEW.status != OLD.status THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'status',
        old_data = OLD.status,
        new_data = NEW.status,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.regist_time IS NULL AND NEW.regist_time IS NOT NULL) OR
       (OLD.regist_time IS NOT NULL AND NEW.regist_time IS NULL) OR
       NEW.regist_time != OLD.regist_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'regist_time',
        old_data = OLD.regist_time,
        new_data = NEW.regist_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.update_time IS NULL AND NEW.update_time IS NOT NULL) OR
       (OLD.update_time IS NOT NULL AND NEW.update_time IS NULL) OR
       NEW.update_time != OLD.update_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'update_time',
        old_data = OLD.update_time,
        new_data = NEW.update_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.photo_upload_time IS NULL AND NEW.photo_upload_time IS NOT NULL) OR
       (OLD.photo_upload_time IS NOT NULL AND NEW.photo_upload_time IS NULL) OR
       NEW.photo_upload_time != OLD.photo_upload_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'photo_upload_time',
        old_data = OLD.photo_upload_time,
        new_data = NEW.photo_upload_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.schedule_update_time IS NULL AND NEW.schedule_update_time IS NOT NULL) OR
       (OLD.schedule_update_time IS NOT NULL AND NEW.schedule_update_time IS NULL) OR
       NEW.schedule_update_time != OLD.schedule_update_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'schedule_update_time',
        old_data = OLD.schedule_update_time,
        new_data = NEW.schedule_update_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.settings_update_time IS NULL AND NEW.settings_update_time IS NOT NULL) OR
       (OLD.settings_update_time IS NOT NULL AND NEW.settings_update_time IS NULL) OR
       NEW.settings_update_time != OLD.settings_update_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'settings_update_time',
        old_data = OLD.settings_update_time,
        new_data = NEW.settings_update_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.activate_time IS NULL AND NEW.activate_time IS NOT NULL) OR
       (OLD.activate_time IS NOT NULL AND NEW.activate_time IS NULL) OR
       NEW.activate_time != OLD.activate_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'activate_time',
        old_data = OLD.activate_time,
        new_data = NEW.activate_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.stop_time IS NULL AND NEW.stop_time IS NOT NULL) OR
       (OLD.stop_time IS NOT NULL AND NEW.stop_time IS NULL) OR
       NEW.stop_time != OLD.stop_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'stop_time',
        old_data = OLD.stop_time,
        new_data = NEW.stop_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.release_time IS NULL AND NEW.release_time IS NOT NULL) OR
       (OLD.release_time IS NOT NULL AND NEW.release_time IS NULL) OR
       NEW.release_time != OLD.release_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'release_time',
        old_data = OLD.release_time,
        new_data = NEW.release_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.is_hourly IS NULL AND NEW.is_hourly IS NOT NULL) OR
       (OLD.is_hourly IS NOT NULL AND NEW.is_hourly IS NULL) OR
       NEW.is_hourly != OLD.is_hourly THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'is_hourly',
        old_data = OLD.is_hourly,
        new_data = NEW.is_hourly,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.fee_per_unit IS NULL AND NEW.fee_per_unit IS NOT NULL) OR
       (OLD.fee_per_unit IS NOT NULL AND NEW.fee_per_unit IS NULL) OR
       NEW.fee_per_unit != OLD.fee_per_unit THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'fee_per_unit',
        old_data = OLD.fee_per_unit,
        new_data = NEW.fee_per_unit,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.maximum_price_type IS NULL AND NEW.maximum_price_type IS NOT NULL) OR
       (OLD.maximum_price_type IS NOT NULL AND NEW.maximum_price_type IS NULL) OR
       NEW.maximum_price_type != OLD.maximum_price_type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'maximum_price_type',
        old_data = OLD.maximum_price_type,
        new_data = NEW.maximum_price_type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.can_go_inout IS NULL AND NEW.can_go_inout IS NOT NULL) OR
       (OLD.can_go_inout IS NOT NULL AND NEW.can_go_inout IS NULL) OR
       NEW.can_go_inout != OLD.can_go_inout THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'can_go_inout',
        old_data = OLD.can_go_inout,
        new_data = NEW.can_go_inout,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.area_type IS NULL AND NEW.area_type IS NOT NULL) OR
       (OLD.area_type IS NOT NULL AND NEW.area_type IS NULL) OR
       NEW.area_type != OLD.area_type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'area_type',
        old_data = OLD.area_type,
        new_data = NEW.area_type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.contract_type IS NULL AND NEW.contract_type IS NOT NULL) OR
       (OLD.contract_type IS NOT NULL AND NEW.contract_type IS NULL) OR
       NEW.contract_type != OLD.contract_type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'contract_type',
        old_data = OLD.contract_type,
        new_data = NEW.contract_type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.car_number_required IS NULL AND NEW.car_number_required IS NOT NULL) OR
       (OLD.car_number_required IS NOT NULL AND NEW.car_number_required IS NULL) OR
       NEW.car_number_required != OLD.car_number_required THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'car_number_required',
        old_data = OLD.car_number_required,
        new_data = NEW.car_number_required,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.is_today_unavailable IS NULL AND NEW.is_today_unavailable IS NOT NULL) OR
       (OLD.is_today_unavailable IS NOT NULL AND NEW.is_today_unavailable IS NULL) OR
       NEW.is_today_unavailable != OLD.is_today_unavailable THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'is_today_unavailable',
        old_data = OLD.is_today_unavailable,
        new_data = NEW.is_today_unavailable,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_count IS NULL AND NEW.rating_count IS NOT NULL) OR
       (OLD.rating_count IS NOT NULL AND NEW.rating_count IS NULL) OR
       NEW.rating_count != OLD.rating_count THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_count',
        old_data = OLD.rating_count,
        new_data = NEW.rating_count,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating IS NULL AND NEW.rating IS NOT NULL) OR
       (OLD.rating IS NOT NULL AND NEW.rating IS NULL) OR
       NEW.rating != OLD.rating THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating',
        old_data = OLD.rating,
        new_data = NEW.rating,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_stars IS NULL AND NEW.rating_stars IS NOT NULL) OR
       (OLD.rating_stars IS NOT NULL AND NEW.rating_stars IS NULL) OR
       NEW.rating_stars != OLD.rating_stars THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_stars',
        old_data = OLD.rating_stars,
        new_data = NEW.rating_stars,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_location IS NULL AND NEW.rating_location IS NOT NULL) OR
       (OLD.rating_location IS NOT NULL AND NEW.rating_location IS NULL) OR
       NEW.rating_location != OLD.rating_location THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_location',
        old_data = OLD.rating_location,
        new_data = NEW.rating_location,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_space IS NULL AND NEW.rating_space IS NOT NULL) OR
       (OLD.rating_space IS NOT NULL AND NEW.rating_space IS NULL) OR
       NEW.rating_space != OLD.rating_space THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_space',
        old_data = OLD.rating_space,
        new_data = NEW.rating_space,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_price IS NULL AND NEW.rating_price IS NOT NULL) OR
       (OLD.rating_price IS NOT NULL AND NEW.rating_price IS NULL) OR
       NEW.rating_price != OLD.rating_price THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_price',
        old_data = OLD.rating_price,
        new_data = NEW.rating_price,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.rating_update_time IS NULL AND NEW.rating_update_time IS NOT NULL) OR
       (OLD.rating_update_time IS NOT NULL AND NEW.rating_update_time IS NULL) OR
       NEW.rating_update_time != OLD.rating_update_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'rating_update_time',
        old_data = OLD.rating_update_time,
        new_data = NEW.rating_update_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.available_days IS NULL AND NEW.available_days IS NOT NULL) OR
       (OLD.available_days IS NOT NULL AND NEW.available_days IS NULL) OR
       NEW.available_days != OLD.available_days THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'available_days',
        old_data = OLD.available_days,
        new_data = NEW.available_days,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.new_available_days IS NULL AND NEW.new_available_days IS NOT NULL) OR
       (OLD.new_available_days IS NOT NULL AND NEW.new_available_days IS NULL) OR
       NEW.new_available_days != OLD.new_available_days THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'new_available_days',
        old_data = OLD.new_available_days,
        new_data = NEW.new_available_days,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.external_parking_id IS NULL AND NEW.external_parking_id IS NOT NULL) OR
       (OLD.external_parking_id IS NOT NULL AND NEW.external_parking_id IS NULL) OR
       NEW.external_parking_id != OLD.external_parking_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'external_parking_id',
        old_data = OLD.external_parking_id,
        new_data = NEW.external_parking_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.reservation_one_day_only IS NULL AND NEW.reservation_one_day_only IS NOT NULL) OR
       (OLD.reservation_one_day_only IS NOT NULL AND NEW.reservation_one_day_only IS NULL) OR
       NEW.reservation_one_day_only != OLD.reservation_one_day_only THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_id,
        table_name = 'parking',
        column_name = 'reservation_one_day_only',
        old_data = OLD.reservation_one_day_only,
        new_data = NEW.reservation_one_day_only,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
  END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ parking_available_date
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_available_date`;

CREATE TABLE `parking_available_date` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` varchar(2000) DEFAULT '',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_available_hour
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_available_hour`;

CREATE TABLE `parking_available_hour` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `available` tinyint(1) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`parking_space_id`,`start_at`),
  KEY `idx_parking_date_space_start` (`parking_id`,`date`,`parking_space_id`,`start_at`),
  KEY `idx_test` (`date`,`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_available_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_available_info`;

CREATE TABLE `parking_available_info` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `price` int(11) NOT NULL,
  `available` int(11) DEFAULT NULL,
  `update_time` datetime NOT NULL,
  `fee_per_unit` int(11) DEFAULT NULL,
  PRIMARY KEY (`parking_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_invited_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_invited_info`;

CREATE TABLE `parking_invited_info` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `invite_id` varchar(100) NOT NULL DEFAULT '',
  `campaign_id` varchar(100) DEFAULT NULL,
  `special_pay_rate` int(11) DEFAULT NULL,
  `special_pay_rate_start_date` date DEFAULT NULL,
  `special_pay_rate_end_date` date DEFAULT NULL,
  `use_time` datetime DEFAULT NULL,
  `present_send_time` datetime DEFAULT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_nearest_station
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_nearest_station`;

CREATE TABLE `parking_nearest_station` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `traveltime` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_organization`;

CREATE TABLE `parking_organization` (
  `parking_id` varchar(100) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`parking_id`),
  KEY `organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_photo`;

CREATE TABLE `parking_photo` (
  `parking_photo_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL,
  `photo_name` varchar(200) NOT NULL DEFAULT '',
  `photo_caption` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`parking_photo_id`),
  UNIQUE KEY `photo_name` (`photo_name`),
  KEY `idx_parking_id` (`parking_id`),
  KEY `idx_parking_id_status` (`parking_id`,`status`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `insert_parking_photo` AFTER INSERT ON `parking_photo` FOR EACH ROW BEGIN

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'parking_photo_id',
    new_data = NEW.parking_photo_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'parking_id',
    new_data = NEW.parking_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'number',
    new_data = NEW.number,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'photo_name',
    new_data = NEW.photo_name,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'photo_caption',
    new_data = NEW.photo_caption,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'status',
    new_data = NEW.status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'regist_time',
    new_data = NEW.regist_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'update_time',
    new_data = NEW.update_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_parking_photo` AFTER UPDATE ON `parking_photo` FOR EACH ROW BEGIN

  IF (OLD.parking_photo_id IS NULL AND NEW.parking_photo_id IS NOT NULL) OR 
     (OLD.parking_photo_id IS NOT NULL AND NEW.parking_photo_id IS NULL) OR 
     NEW.parking_photo_id != OLD.parking_photo_id THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'parking_photo_id',
    old_data = OLD.parking_photo_id,
    new_data = NEW.parking_photo_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.parking_id IS NULL AND NEW.parking_id IS NOT NULL) OR 
     (OLD.parking_id IS NOT NULL AND NEW.parking_id IS NULL) OR 
     NEW.parking_id != OLD.parking_id THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'parking_id',
    old_data = OLD.parking_id,
    new_data = NEW.parking_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.number IS NULL AND NEW.number IS NOT NULL) OR 
     (OLD.number IS NOT NULL AND NEW.number IS NULL) OR 
     NEW.number != OLD.number THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'number',
    old_data = OLD.number,
    new_data = NEW.number,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.photo_name IS NULL AND NEW.photo_name IS NOT NULL) OR 
     (OLD.photo_name IS NOT NULL AND NEW.photo_name IS NULL) OR 
     NEW.photo_name != OLD.photo_name THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'photo_name',
    old_data = OLD.photo_name,
    new_data = NEW.photo_name,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.photo_caption IS NULL AND NEW.photo_caption IS NOT NULL) OR 
     (OLD.photo_caption IS NOT NULL AND NEW.photo_caption IS NULL) OR 
     NEW.photo_caption != OLD.photo_caption THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'photo_caption',
    old_data = OLD.photo_caption,
    new_data = NEW.photo_caption,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR 
     (OLD.status IS NOT NULL AND NEW.status IS NULL) OR 
     NEW.status != OLD.status THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'status',
    old_data = OLD.status,
    new_data = NEW.status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.regist_time IS NULL AND NEW.regist_time IS NOT NULL) OR 
     (OLD.regist_time IS NOT NULL AND NEW.regist_time IS NULL) OR 
     NEW.regist_time != OLD.regist_time THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'regist_time',
    old_data = OLD.regist_time,
    new_data = NEW.regist_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.update_time IS NULL AND NEW.update_time IS NOT NULL) OR 
     (OLD.update_time IS NOT NULL AND NEW.update_time IS NULL) OR 
     NEW.update_time != OLD.update_time THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_photo_id,
    table_name = 'parking_photo',
    column_name = 'update_time',
    old_data = OLD.update_time,
    new_data = NEW.update_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ parking_pin_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_pin_groups`;

CREATE TABLE `parking_pin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_sales_manage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_sales_manage`;

CREATE TABLE `parking_sales_manage` (
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `channel` varchar(200) DEFAULT NULL,
  `association` varchar(200) DEFAULT NULL,
  `owner` varchar(200) DEFAULT NULL,
  `parking_type` varchar(200) DEFAULT NULL,
  `place_type` varchar(200) DEFAULT NULL,
  `facility` varchar(200) DEFAULT NULL,
  `manager` varchar(200) DEFAULT NULL,
  `cp_type` varchar(200) DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL,
  `area_rank` varchar(200) DEFAULT NULL,
  `fixture` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_settings`;

CREATE TABLE `parking_settings` (
  `parking_settings_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) DEFAULT NULL,
  `everyday` int(11) DEFAULT NULL,
  `wday` int(11) DEFAULT NULL,
  `holiday` tinyint(4) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `exclude_holiday` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_owner` int(11) DEFAULT NULL,
  `pay_rate` int(11) DEFAULT NULL,
  `remarks` text,
  `status` varchar(20) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  `fee_per_unit` int(10) unsigned NOT NULL,
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`parking_settings_id`),
  KEY `idx_parkingid_status_order` (`parking_id`,`status`,`order`),
  KEY `idx_status_order` (`status`,`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `insert_parking_settings` AFTER INSERT ON `parking_settings` FOR EACH ROW BEGIN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'parking_settings_id',
new_data = NEW.parking_settings_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'parking_id',
new_data = NEW.parking_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'everyday',
new_data = NEW.everyday,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'wday',
new_data = NEW.wday,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'holiday',
new_data = NEW.holiday,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'date',
new_data = NEW.date,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'exclude_holiday',
new_data = NEW.exclude_holiday,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'price',
new_data = NEW.price,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'price_owner',
new_data = NEW.price_owner,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'pay_rate',
new_data = NEW.pay_rate,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'status',
new_data = NEW.status,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'order',
new_data = NEW.order,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'update_time',
new_data = NEW.update_time,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'regist_time',
new_data = NEW.regist_time,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'fee_per_unit',
new_data = NEW.fee_per_unit,
update_user_id = NEW.update_user_id,
created_at = NOW();
END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_parking_settings` AFTER UPDATE ON `parking_settings` FOR EACH ROW BEGIN
IF (OLD.parking_settings_id IS NULL AND NEW.parking_settings_id IS NOT NULL) OR 
(OLD.parking_settings_id IS NOT NULL AND NEW.parking_settings_id IS NULL) OR 
NEW.parking_settings_id != OLD.parking_settings_id THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'parking_settings_id',
old_data = OLD.parking_settings_id,
new_data = NEW.parking_settings_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.parking_id IS NULL AND NEW.parking_id IS NOT NULL) OR 
(OLD.parking_id IS NOT NULL AND NEW.parking_id IS NULL) OR 
NEW.parking_id != OLD.parking_id THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'parking_id',
old_data = OLD.parking_id,
new_data = NEW.parking_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.everyday IS NULL AND NEW.everyday IS NOT NULL) OR 
(OLD.everyday IS NOT NULL AND NEW.everyday IS NULL) OR 
NEW.everyday != OLD.everyday THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'everyday',
old_data = OLD.everyday,
new_data = NEW.everyday,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.wday IS NULL AND NEW.wday IS NOT NULL) OR 
(OLD.wday IS NOT NULL AND NEW.wday IS NULL) OR 
NEW.wday != OLD.wday THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'wday',
old_data = OLD.wday,
new_data = NEW.wday,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.holiday IS NULL AND NEW.holiday IS NOT NULL) OR 
(OLD.holiday IS NOT NULL AND NEW.holiday IS NULL) OR 
NEW.holiday != OLD.holiday THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'holiday',
old_data = OLD.holiday,
new_data = NEW.holiday,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.date IS NULL AND NEW.date IS NOT NULL) OR 
(OLD.date IS NOT NULL AND NEW.date IS NULL) OR 
NEW.date != OLD.date THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'date',
old_data = OLD.date,
new_data = NEW.date,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.exclude_holiday IS NULL AND NEW.exclude_holiday IS NOT NULL) OR 
(OLD.exclude_holiday IS NOT NULL AND NEW.exclude_holiday IS NULL) OR 
NEW.exclude_holiday != OLD.exclude_holiday THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'exclude_holiday',
old_data = OLD.exclude_holiday,
new_data = NEW.exclude_holiday,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.price IS NULL AND NEW.price IS NOT NULL) OR 
(OLD.price IS NOT NULL AND NEW.price IS NULL) OR 
NEW.price != OLD.price THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'price',
old_data = OLD.price,
new_data = NEW.price,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.price_owner IS NULL AND NEW.price_owner IS NOT NULL) OR 
(OLD.price_owner IS NOT NULL AND NEW.price_owner IS NULL) OR 
NEW.price_owner != OLD.price_owner THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'price_owner',
old_data = OLD.price_owner,
new_data = NEW.price_owner,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.pay_rate IS NULL AND NEW.pay_rate IS NOT NULL) OR 
(OLD.pay_rate IS NOT NULL AND NEW.pay_rate IS NULL) OR 
NEW.pay_rate != OLD.pay_rate THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'pay_rate',
old_data = OLD.pay_rate,
new_data = NEW.pay_rate,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR 
(OLD.status IS NOT NULL AND NEW.status IS NULL) OR 
NEW.status != OLD.status THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'status',
old_data = OLD.status,
new_data = NEW.status,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.order IS NULL AND NEW.order IS NOT NULL) OR 
(OLD.order IS NOT NULL AND NEW.order IS NULL) OR 
NEW.order != OLD.order THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'order',
old_data = OLD.order,
new_data = NEW.order,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.update_time IS NULL AND NEW.update_time IS NOT NULL) OR 
(OLD.update_time IS NOT NULL AND NEW.update_time IS NULL) OR 
NEW.update_time != OLD.update_time THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'update_time',
old_data = OLD.update_time,
new_data = NEW.update_time,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.regist_time IS NULL AND NEW.regist_time IS NOT NULL) OR 
(OLD.regist_time IS NOT NULL AND NEW.regist_time IS NULL) OR 
NEW.regist_time != OLD.regist_time THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'regist_time',
old_data = OLD.regist_time,
new_data = NEW.regist_time,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.fee_per_unit IS NULL AND NEW.fee_per_unit IS NOT NULL) OR 
(OLD.fee_per_unit IS NOT NULL AND NEW.fee_per_unit IS NULL) OR 
NEW.fee_per_unit != OLD.fee_per_unit THEN
INSERT INTO update_history SET
uniq_id = NEW.parking_settings_id,
table_name = 'parking_settings',
column_name = 'fee_per_unit',
old_data = OLD.fee_per_unit,
new_data = NEW.fee_per_unit,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ parking_settings_snapshot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_settings_snapshot`;

CREATE TABLE `parking_settings_snapshot` (
  `snapshot_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `reservation_changed_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `price` int(11) DEFAULT NULL,
  `price_owner` int(11) DEFAULT NULL,
  `fee_per_unit` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`snapshot_id`),
  KEY `idx_reservationid_date` (`reservation_id`,`date`),
  KEY `idx_extended_date` (`reservation_changed_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_space
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_space`;

CREATE TABLE `parking_space` (
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(1000) DEFAULT NULL COMMENT '駐車スペース名、番号',
  `explanation` text,
  `car_type` varchar(100) DEFAULT NULL,
  `car_types` set('オートバイ','軽自動車','コンパクトカー','中型車','ワンボックス','大型車・SUV') DEFAULT NULL,
  `highroof` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `nobike` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL COMMENT 'mm',
  `width` int(11) DEFAULT NULL COMMENT 'mm',
  `length` int(11) DEFAULT NULL COMMENT 'mm',
  `weight` int(11) DEFAULT NULL COMMENT 'kg',
  `under` int(11) DEFAULT NULL,
  `tire` int(11) DEFAULT NULL,
  `image1` varchar(200) DEFAULT '',
  `note` varchar(2000) DEFAULT NULL,
  `staff_comment` text,
  `status` varchar(20) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `photo_upload_time` datetime DEFAULT NULL,
  `activate_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `manage_id` varchar(100) DEFAULT NULL,
  `active_reservation_date` date DEFAULT NULL,
  `stop_reservation_date` date DEFAULT NULL,
  `external_parking_space_id` varchar(100) DEFAULT NULL,
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`parking_space_id`),
  KEY `idx_parking_id` (`parking_id`),
  KEY `idx_parkingid_nobike_status` (`parking_id`,`nobike`,`status`),
  KEY `idx_parkingid_cartypes_status` (`parking_id`,`car_types`,`status`),
  KEY `idx_status_parkingid` (`status`,`parking_id`),
  KEY `idx_cooperative_key` (`external_parking_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_parking_space` AFTER UPDATE ON `parking_space` FOR EACH ROW BEGIN
    IF (OLD.parking_space_id IS NULL AND NEW.parking_space_id IS NOT NULL) OR
       (OLD.parking_space_id IS NOT NULL AND NEW.parking_space_id IS NULL) OR
       NEW.parking_space_id != OLD.parking_space_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'parking_space_id',
        old_data = OLD.parking_space_id,
        new_data = NEW.parking_space_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.parking_id IS NULL AND NEW.parking_id IS NOT NULL) OR
       (OLD.parking_id IS NOT NULL AND NEW.parking_id IS NULL) OR
       NEW.parking_id != OLD.parking_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'parking_id',
        old_data = OLD.parking_id,
        new_data = NEW.parking_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.name IS NULL AND NEW.name IS NOT NULL) OR
       (OLD.name IS NOT NULL AND NEW.name IS NULL) OR
       NEW.name != OLD.name THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'name',
        old_data = OLD.name,
        new_data = NEW.name,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.explanation IS NULL AND NEW.explanation IS NOT NULL) OR
       (OLD.explanation IS NOT NULL AND NEW.explanation IS NULL) OR
       NEW.explanation != OLD.explanation THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'explanation',
        old_data = OLD.explanation,
        new_data = NEW.explanation,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.car_type IS NULL AND NEW.car_type IS NOT NULL) OR
       (OLD.car_type IS NOT NULL AND NEW.car_type IS NULL) OR
       NEW.car_type != OLD.car_type THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'car_type',
        old_data = OLD.car_type,
        new_data = NEW.car_type,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.car_types IS NULL AND NEW.car_types IS NOT NULL) OR
       (OLD.car_types IS NOT NULL AND NEW.car_types IS NULL) OR
       NEW.car_types != OLD.car_types THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'car_types',
        old_data = OLD.car_types,
        new_data = NEW.car_types,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.highroof IS NULL AND NEW.highroof IS NOT NULL) OR
       (OLD.highroof IS NOT NULL AND NEW.highroof IS NULL) OR
       NEW.highroof != OLD.highroof THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'highroof',
        old_data = OLD.highroof,
        new_data = NEW.highroof,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.score IS NULL AND NEW.score IS NOT NULL) OR
       (OLD.score IS NOT NULL AND NEW.score IS NULL) OR
       NEW.score != OLD.score THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'score',
        old_data = OLD.score,
        new_data = NEW.score,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.nobike IS NULL AND NEW.nobike IS NOT NULL) OR
       (OLD.nobike IS NOT NULL AND NEW.nobike IS NULL) OR
       NEW.nobike != OLD.nobike THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'nobike',
        old_data = OLD.nobike,
        new_data = NEW.nobike,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.height IS NULL AND NEW.height IS NOT NULL) OR
       (OLD.height IS NOT NULL AND NEW.height IS NULL) OR
       NEW.height != OLD.height THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'height',
        old_data = OLD.height,
        new_data = NEW.height,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.width IS NULL AND NEW.width IS NOT NULL) OR
       (OLD.width IS NOT NULL AND NEW.width IS NULL) OR
       NEW.width != OLD.width THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'width',
        old_data = OLD.width,
        new_data = NEW.width,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.length IS NULL AND NEW.length IS NOT NULL) OR
       (OLD.length IS NOT NULL AND NEW.length IS NULL) OR
       NEW.length != OLD.length THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'length',
        old_data = OLD.length,
        new_data = NEW.length,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.weight IS NULL AND NEW.weight IS NOT NULL) OR
       (OLD.weight IS NOT NULL AND NEW.weight IS NULL) OR
       NEW.weight != OLD.weight THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'weight',
        old_data = OLD.weight,
        new_data = NEW.weight,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.under IS NULL AND NEW.under IS NOT NULL) OR
       (OLD.under IS NOT NULL AND NEW.under IS NULL) OR
       NEW.under != OLD.under THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'under',
        old_data = OLD.under,
        new_data = NEW.under,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.tire IS NULL AND NEW.tire IS NOT NULL) OR
       (OLD.tire IS NOT NULL AND NEW.tire IS NULL) OR
       NEW.tire != OLD.tire THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'tire',
        old_data = OLD.tire,
        new_data = NEW.tire,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.image1 IS NULL AND NEW.image1 IS NOT NULL) OR
       (OLD.image1 IS NOT NULL AND NEW.image1 IS NULL) OR
       NEW.image1 != OLD.image1 THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'image1',
        old_data = OLD.image1,
        new_data = NEW.image1,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.note IS NULL AND NEW.note IS NOT NULL) OR
       (OLD.note IS NOT NULL AND NEW.note IS NULL) OR
       NEW.note != OLD.note THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'note',
        old_data = OLD.note,
        new_data = NEW.note,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.staff_comment IS NULL AND NEW.staff_comment IS NOT NULL) OR
       (OLD.staff_comment IS NOT NULL AND NEW.staff_comment IS NULL) OR
       NEW.staff_comment != OLD.staff_comment THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'staff_comment',
        old_data = OLD.staff_comment,
        new_data = NEW.staff_comment,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR
       (OLD.status IS NOT NULL AND NEW.status IS NULL) OR
       NEW.status != OLD.status THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'status',
        old_data = OLD.status,
        new_data = NEW.status,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.regist_time IS NULL AND NEW.regist_time IS NOT NULL) OR
       (OLD.regist_time IS NOT NULL AND NEW.regist_time IS NULL) OR
       NEW.regist_time != OLD.regist_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'regist_time',
        old_data = OLD.regist_time,
        new_data = NEW.regist_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.update_time IS NULL AND NEW.update_time IS NOT NULL) OR
       (OLD.update_time IS NOT NULL AND NEW.update_time IS NULL) OR
       NEW.update_time != OLD.update_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'update_time',
        old_data = OLD.update_time,
        new_data = NEW.update_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.photo_upload_time IS NULL AND NEW.photo_upload_time IS NOT NULL) OR
       (OLD.photo_upload_time IS NOT NULL AND NEW.photo_upload_time IS NULL) OR
       NEW.photo_upload_time != OLD.photo_upload_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'photo_upload_time',
        old_data = OLD.photo_upload_time,
        new_data = NEW.photo_upload_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.activate_time IS NULL AND NEW.activate_time IS NOT NULL) OR
       (OLD.activate_time IS NOT NULL AND NEW.activate_time IS NULL) OR
       NEW.activate_time != OLD.activate_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'activate_time',
        old_data = OLD.activate_time,
        new_data = NEW.activate_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.stop_time IS NULL AND NEW.stop_time IS NOT NULL) OR
       (OLD.stop_time IS NOT NULL AND NEW.stop_time IS NULL) OR
       NEW.stop_time != OLD.stop_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'stop_time',
        old_data = OLD.stop_time,
        new_data = NEW.stop_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.release_time IS NULL AND NEW.release_time IS NOT NULL) OR
       (OLD.release_time IS NOT NULL AND NEW.release_time IS NULL) OR
       NEW.release_time != OLD.release_time THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'release_time',
        old_data = OLD.release_time,
        new_data = NEW.release_time,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.manage_id IS NULL AND NEW.manage_id IS NOT NULL) OR
       (OLD.manage_id IS NOT NULL AND NEW.manage_id IS NULL) OR
       NEW.manage_id != OLD.manage_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'manage_id',
        old_data = OLD.manage_id,
        new_data = NEW.manage_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.active_reservation_date IS NULL AND NEW.active_reservation_date IS NOT NULL) OR
       (OLD.active_reservation_date IS NOT NULL AND NEW.active_reservation_date IS NULL) OR
       NEW.active_reservation_date != OLD.active_reservation_date THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'active_reservation_date',
        old_data = OLD.active_reservation_date,
        new_data = NEW.active_reservation_date,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.stop_reservation_date IS NULL AND NEW.stop_reservation_date IS NOT NULL) OR
       (OLD.stop_reservation_date IS NOT NULL AND NEW.stop_reservation_date IS NULL) OR
       NEW.stop_reservation_date != OLD.stop_reservation_date THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'stop_reservation_date',
        old_data = OLD.stop_reservation_date,
        new_data = NEW.stop_reservation_date,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
    IF (OLD.external_parking_space_id IS NULL AND NEW.external_parking_space_id IS NOT NULL) OR
       (OLD.external_parking_space_id IS NOT NULL AND NEW.external_parking_space_id IS NULL) OR
       NEW.external_parking_space_id != OLD.external_parking_space_id THEN
      INSERT INTO update_history SET
        uniq_id = NEW.parking_space_id,
        table_name = 'parking_space',
        column_name = 'external_parking_space_id',
        old_data = OLD.external_parking_space_id,
        new_data = NEW.external_parking_space_id,
        update_user_id = NEW.update_user_id,
        created_at = NOW();
    END IF;
  END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ parking_space_status_request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_space_status_request`;

CREATE TABLE `parking_space_status_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_id` varchar(100) NOT NULL,
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `request_type` tinyint(4) NOT NULL,
  `request_comment` varchar(200) DEFAULT NULL,
  `request_status` tinyint(4) NOT NULL,
  `request_user_id` varchar(100) NOT NULL,
  `update_user_id` varchar(100) NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `idx_parking_id` (`parking_id`),
  KEY `idx_parking_space_id` (`parking_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_space_stop_reason
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_space_stop_reason`;

CREATE TABLE `parking_space_stop_reason` (
  `parking_id` varchar(100) NOT NULL,
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `stop_reason_type` tinyint(4) NOT NULL,
  `reason_comment` varchar(200) DEFAULT NULL,
  `update_user_id` varchar(100) NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  KEY `idx_parking_id` (`parking_id`),
  KEY `idx_parking_space_id` (`parking_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_special_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_special_service`;

CREATE TABLE `parking_special_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `information_page_url` varchar(1000) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_special_service_application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_special_service_application`;

CREATE TABLE `parking_special_service_application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ parking_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parking_status`;

CREATE TABLE `parking_status` (
  `parking_id` varchar(100) NOT NULL,
  `is_automated_parking` tinyint(1) DEFAULT NULL,
  `has_roof` tinyint(1) DEFAULT NULL,
  `available_24h` tinyint(1) DEFAULT NULL,
  `street_type` enum('wide','narrow','normal') DEFAULT 'normal',
  `has_route_claims` tinyint(1) DEFAULT NULL,
  `is_mansion` tinyint(1) DEFAULT NULL,
  `is_monthly` tinyint(1) DEFAULT NULL,
  `owner_type` enum('individual','corporation') DEFAULT 'individual',
  `is_easy_to_recognize` tinyint(1) DEFAULT NULL,
  `warning_status` enum('blue','yellow','red','black') DEFAULT 'blue',
  `has_wall` tinyint(1) DEFAULT NULL,
  `has_gate` tinyint(1) DEFAULT NULL,
  `has_car_stop` tinyint(1) DEFAULT NULL,
  `has_grass` tinyint(1) DEFAULT NULL,
  `has_slope` tinyint(1) DEFAULT NULL,
  `has_uneven_ground` tinyint(1) DEFAULT NULL,
  `has_gravel` tinyint(1) DEFAULT NULL,
  `is_home` tinyint(1) DEFAULT NULL,
  `detail_memo` text,
  `update_time` datetime DEFAULT NULL,
  `acquisition_route` varchar(40) NOT NULL DEFAULT '',
  `acquisition_info` text NOT NULL,
  `acquisition_memo` text NOT NULL,
  `parking_type` tinyint(1) DEFAULT NULL,
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`parking_id`),
  KEY `idx_24h` (`available_24h`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `insert_parking_status` AFTER INSERT ON `parking_status` FOR EACH ROW BEGIN

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'parking_id',
    new_data = NEW.parking_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_automated_parking',
    new_data = NEW.is_automated_parking,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_roof',
    new_data = NEW.has_roof,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'available_24h',
    new_data = NEW.available_24h,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'street_type',
    new_data = NEW.street_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_route_claims',
    new_data = NEW.has_route_claims,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_mansion',
    new_data = NEW.is_mansion,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_monthly',
    new_data = NEW.is_monthly,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'owner_type',
    new_data = NEW.owner_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_easy_to_recognize',
    new_data = NEW.is_easy_to_recognize,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'warning_status',
    new_data = NEW.warning_status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_wall',
    new_data = NEW.has_wall,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_gate',
    new_data = NEW.has_gate,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_car_stop',
    new_data = NEW.has_car_stop,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_grass',
    new_data = NEW.has_grass,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_slope',
    new_data = NEW.has_slope,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_uneven_ground',
    new_data = NEW.has_uneven_ground,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_gravel',
    new_data = NEW.has_gravel,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_home',
    new_data = NEW.is_home,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'detail_memo',
    new_data = NEW.detail_memo,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'update_time',
    new_data = NEW.update_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_route',
    new_data = NEW.acquisition_route,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_info',
    new_data = NEW.acquisition_info,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_memo',
    new_data = NEW.acquisition_memo,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'parking_type',
    new_data = NEW.parking_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();

END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_parking_status` AFTER UPDATE ON `parking_status` FOR EACH ROW BEGIN

  IF (OLD.parking_id IS NULL AND NEW.parking_id IS NOT NULL) OR 
     (OLD.parking_id IS NOT NULL AND NEW.parking_id IS NULL) OR 
     NEW.parking_id != OLD.parking_id THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'parking_id',
    old_data = OLD.parking_id,
    new_data = NEW.parking_id,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.is_automated_parking IS NULL AND NEW.is_automated_parking IS NOT NULL) OR 
     (OLD.is_automated_parking IS NOT NULL AND NEW.is_automated_parking IS NULL) OR 
     NEW.is_automated_parking != OLD.is_automated_parking THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_automated_parking',
    old_data = OLD.is_automated_parking,
    new_data = NEW.is_automated_parking,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_roof IS NULL AND NEW.has_roof IS NOT NULL) OR 
     (OLD.has_roof IS NOT NULL AND NEW.has_roof IS NULL) OR 
     NEW.has_roof != OLD.has_roof THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_roof',
    old_data = OLD.has_roof,
    new_data = NEW.has_roof,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.available_24h IS NULL AND NEW.available_24h IS NOT NULL) OR 
     (OLD.available_24h IS NOT NULL AND NEW.available_24h IS NULL) OR 
     NEW.available_24h != OLD.available_24h THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'available_24h',
    old_data = OLD.available_24h,
    new_data = NEW.available_24h,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.street_type IS NULL AND NEW.street_type IS NOT NULL) OR 
     (OLD.street_type IS NOT NULL AND NEW.street_type IS NULL) OR 
     NEW.street_type != OLD.street_type THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'street_type',
    old_data = OLD.street_type,
    new_data = NEW.street_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_route_claims IS NULL AND NEW.has_route_claims IS NOT NULL) OR 
     (OLD.has_route_claims IS NOT NULL AND NEW.has_route_claims IS NULL) OR 
     NEW.has_route_claims != OLD.has_route_claims THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_route_claims',
    old_data = OLD.has_route_claims,
    new_data = NEW.has_route_claims,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.is_mansion IS NULL AND NEW.is_mansion IS NOT NULL) OR 
     (OLD.is_mansion IS NOT NULL AND NEW.is_mansion IS NULL) OR 
     NEW.is_mansion != OLD.is_mansion THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_mansion',
    old_data = OLD.is_mansion,
    new_data = NEW.is_mansion,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.is_monthly IS NULL AND NEW.is_monthly IS NOT NULL) OR 
     (OLD.is_monthly IS NOT NULL AND NEW.is_monthly IS NULL) OR 
     NEW.is_monthly != OLD.is_monthly THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_monthly',
    old_data = OLD.is_monthly,
    new_data = NEW.is_monthly,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.owner_type IS NULL AND NEW.owner_type IS NOT NULL) OR 
     (OLD.owner_type IS NOT NULL AND NEW.owner_type IS NULL) OR 
     NEW.owner_type != OLD.owner_type THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'owner_type',
    old_data = OLD.owner_type,
    new_data = NEW.owner_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.is_easy_to_recognize IS NULL AND NEW.is_easy_to_recognize IS NOT NULL) OR 
     (OLD.is_easy_to_recognize IS NOT NULL AND NEW.is_easy_to_recognize IS NULL) OR 
     NEW.is_easy_to_recognize != OLD.is_easy_to_recognize THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_easy_to_recognize',
    old_data = OLD.is_easy_to_recognize,
    new_data = NEW.is_easy_to_recognize,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.warning_status IS NULL AND NEW.warning_status IS NOT NULL) OR 
     (OLD.warning_status IS NOT NULL AND NEW.warning_status IS NULL) OR 
     NEW.warning_status != OLD.warning_status THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'warning_status',
    old_data = OLD.warning_status,
    new_data = NEW.warning_status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_wall IS NULL AND NEW.has_wall IS NOT NULL) OR 
     (OLD.has_wall IS NOT NULL AND NEW.has_wall IS NULL) OR 
     NEW.has_wall != OLD.has_wall THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_wall',
    old_data = OLD.has_wall,
    new_data = NEW.has_wall,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_gate IS NULL AND NEW.has_gate IS NOT NULL) OR 
     (OLD.has_gate IS NOT NULL AND NEW.has_gate IS NULL) OR 
     NEW.has_gate != OLD.has_gate THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_gate',
    old_data = OLD.has_gate,
    new_data = NEW.has_gate,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_car_stop IS NULL AND NEW.has_car_stop IS NOT NULL) OR 
     (OLD.has_car_stop IS NOT NULL AND NEW.has_car_stop IS NULL) OR 
     NEW.has_car_stop != OLD.has_car_stop THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_car_stop',
    old_data = OLD.has_car_stop,
    new_data = NEW.has_car_stop,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_grass IS NULL AND NEW.has_grass IS NOT NULL) OR 
     (OLD.has_grass IS NOT NULL AND NEW.has_grass IS NULL) OR 
     NEW.has_grass != OLD.has_grass THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_grass',
    old_data = OLD.has_grass,
    new_data = NEW.has_grass,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_slope IS NULL AND NEW.has_slope IS NOT NULL) OR 
     (OLD.has_slope IS NOT NULL AND NEW.has_slope IS NULL) OR 
     NEW.has_slope != OLD.has_slope THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_slope',
    old_data = OLD.has_slope,
    new_data = NEW.has_slope,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_uneven_ground IS NULL AND NEW.has_uneven_ground IS NOT NULL) OR 
     (OLD.has_uneven_ground IS NOT NULL AND NEW.has_uneven_ground IS NULL) OR 
     NEW.has_uneven_ground != OLD.has_uneven_ground THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_uneven_ground',
    old_data = OLD.has_uneven_ground,
    new_data = NEW.has_uneven_ground,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.has_gravel IS NULL AND NEW.has_gravel IS NOT NULL) OR 
     (OLD.has_gravel IS NOT NULL AND NEW.has_gravel IS NULL) OR 
     NEW.has_gravel != OLD.has_gravel THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'has_gravel',
    old_data = OLD.has_gravel,
    new_data = NEW.has_gravel,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.is_home IS NULL AND NEW.is_home IS NOT NULL) OR 
     (OLD.is_home IS NOT NULL AND NEW.is_home IS NULL) OR 
     NEW.is_home != OLD.is_home THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'is_home',
    old_data = OLD.is_home,
    new_data = NEW.is_home,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.detail_memo IS NULL AND NEW.detail_memo IS NOT NULL) OR 
     (OLD.detail_memo IS NOT NULL AND NEW.detail_memo IS NULL) OR 
     NEW.detail_memo != OLD.detail_memo THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'detail_memo',
    old_data = OLD.detail_memo,
    new_data = NEW.detail_memo,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.update_time IS NULL AND NEW.update_time IS NOT NULL) OR 
     (OLD.update_time IS NOT NULL AND NEW.update_time IS NULL) OR 
     NEW.update_time != OLD.update_time THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'update_time',
    old_data = OLD.update_time,
    new_data = NEW.update_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.acquisition_route IS NULL AND NEW.acquisition_route IS NOT NULL) OR 
     (OLD.acquisition_route IS NOT NULL AND NEW.acquisition_route IS NULL) OR 
     NEW.acquisition_route != OLD.acquisition_route THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_route',
    old_data = OLD.acquisition_route,
    new_data = NEW.acquisition_route,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.acquisition_info IS NULL AND NEW.acquisition_info IS NOT NULL) OR 
     (OLD.acquisition_info IS NOT NULL AND NEW.acquisition_info IS NULL) OR 
     NEW.acquisition_info != OLD.acquisition_info THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_info',
    old_data = OLD.acquisition_info,
    new_data = NEW.acquisition_info,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.acquisition_memo IS NULL AND NEW.acquisition_memo IS NOT NULL) OR 
     (OLD.acquisition_memo IS NOT NULL AND NEW.acquisition_memo IS NULL) OR 
     NEW.acquisition_memo != OLD.acquisition_memo THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'acquisition_memo',
    old_data = OLD.acquisition_memo,
    new_data = NEW.acquisition_memo,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.parking_type IS NULL AND NEW.parking_type IS NOT NULL) OR 
     (OLD.parking_type IS NOT NULL AND NEW.parking_type IS NULL) OR 
     NEW.parking_type != OLD.parking_type THEN
    INSERT INTO update_history SET
    uniq_id = NEW.parking_id,
    table_name = 'parking_status',
    column_name = 'parking_type',
    old_data = OLD.parking_type,
    new_data = NEW.parking_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ phpmig_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `phpmig_test`;

CREATE TABLE `phpmig_test` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ pre_owner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pre_owner`;

CREATE TABLE `pre_owner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ prefecture
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prefecture`;

CREATE TABLE `prefecture` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(255) NOT NULL,
  `context` text,
  `region_id` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(255) NOT NULL,
  `context` text,
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report`;

CREATE TABLE `report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_count` int(11) DEFAULT NULL,
  `active_user_count` int(11) DEFAULT NULL,
  `leave_user_count` int(11) DEFAULT NULL,
  `reservation_count` int(11) DEFAULT NULL,
  `cancel_reservation_count` int(11) DEFAULT NULL,
  `owner_count` int(11) DEFAULT NULL,
  `active_parking_count` int(11) DEFAULT NULL,
  `daily_parking_count` int(11) DEFAULT NULL,
  `monthly_parking_count` int(11) DEFAULT NULL,
  `active_parking_space_count` int(11) DEFAULT NULL,
  `monthly_count` int(11) DEFAULT NULL,
  `pre_owner_count` int(11) DEFAULT NULL,
  `owner_regist_count` int(11) DEFAULT NULL,
  `inquiry_count` int(11) DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ reservation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation`;

CREATE TABLE `reservation` (
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `base_reservation_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `last_date` date DEFAULT NULL,
  `start_time` varchar(10) NOT NULL DEFAULT '',
  `end_time` varchar(10) NOT NULL DEFAULT '',
  `car_type` varchar(100) DEFAULT NULL,
  `car_number` varchar(100) NOT NULL DEFAULT '',
  `car_model` varchar(100) DEFAULT NULL,
  `unknown_car` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_owner` int(11) DEFAULT NULL,
  `price_total` int(11) DEFAULT NULL,
  `price_owner_total` int(11) DEFAULT NULL,
  `pay_rate` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `melmaga` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `purpose` varchar(1000) DEFAULT NULL,
  `coupon_id` varchar(100) DEFAULT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `gmo_order_id` varchar(27) DEFAULT NULL,
  `gmo_access_id` varchar(32) DEFAULT NULL,
  `gmo_access_password` varchar(32) DEFAULT NULL,
  `gmo_process_date` varchar(14) DEFAULT NULL,
  `ad` tinyint(4) NOT NULL DEFAULT '0',
  `sharegate_type` tinyint(4) DEFAULT '0',
  `trading_id` varchar(100) DEFAULT NULL,
  `payment_id` varchar(100) DEFAULT NULL,
  `gid` varchar(100) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `cancel_amount` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `coupon_amount` int(11) NOT NULL DEFAULT '0',
  `refunded_including_tax` int(11) DEFAULT NULL,
  `nosales` int(11) DEFAULT NULL,
  `nopayment` int(11) DEFAULT NULL,
  `nouse` int(11) DEFAULT NULL,
  `paymentdata` text,
  `staff_comment` text,
  `status` varchar(20) NOT NULL DEFAULT '',
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_career_type` int(11) DEFAULT NULL,
  `app_kind` varchar(10) DEFAULT NULL,
  `coinparking` tinyint(1) DEFAULT NULL,
  `receipt_no` int(11) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  `reserve_time` datetime DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  `payment_notice_time` datetime DEFAULT NULL,
  `capture_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `timeout_time` datetime DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  `notice_time` datetime DEFAULT NULL,
  `cancel_notice_time` datetime DEFAULT NULL,
  `edit_notice_time` datetime DEFAULT NULL,
  `rental_type` varchar(20) NOT NULL DEFAULT 'daily',
  `use_type` varchar(100) DEFAULT NULL,
  `is_extended` tinyint(1) DEFAULT NULL,
  `refunded_point` int(10) unsigned NOT NULL,
  `used_point` int(10) unsigned NOT NULL,
  `checked_out_at` datetime DEFAULT NULL,
  `start_noticed_at` datetime DEFAULT NULL,
  `end_noticed_at` datetime DEFAULT NULL,
  `point_applied_at` datetime DEFAULT NULL,
  `is_modified` tinyint(1) DEFAULT NULL,
  `maximum_price_type` varchar(20) NOT NULL DEFAULT 'per_day',
  `is_checkout_alerted` tinyint(1) DEFAULT NULL,
  `special_delete` tinyint(1) DEFAULT NULL,
  `external_reservation_id` varchar(100) DEFAULT NULL,
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `idx_trading_id` (`trading_id`),
  KEY `idx_userid_status_date` (`user_id`,`status`,`date`),
  KEY `idx_parkingspaceid_status_rentaltype` (`parking_space_id`,`status`,`rental_type`),
  KEY `idx_parkingid_status_rentaltype_checkedoutat` (`parking_id`,`status`,`rental_type`,`checked_out_at`),
  KEY `idx_paymenttype_paymentstatus_capturetime_gid` (`payment_type`,`payment_status`,`capture_time`,`gid`),
  KEY `idx_rentaltype_checkedoutat_ischeckedoutalerted_enddate` (`rental_type`,`checked_out_at`,`is_checkout_alerted`,`end_date`),
  KEY `idx_rentaltype_status_date` (`rental_type`,`status`,`date`),
  KEY `idx_point_applied_at` (`point_applied_at`),
  KEY `idx_email` (`email`),
  KEY `idx_phone_number` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_reservation` AFTER UPDATE ON `reservation` FOR EACH ROW BEGIN

  IF (OLD.date IS NULL AND NEW.date IS NOT NULL) OR 
     (OLD.date IS NOT NULL AND NEW.date IS NULL) OR 
     NEW.date != OLD.date THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'date',
    old_data = OLD.date,
    new_data = NEW.date,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.end_date IS NULL AND NEW.end_date IS NOT NULL) OR 
     (OLD.end_date IS NOT NULL AND NEW.end_date IS NULL) OR 
     NEW.end_date != OLD.end_date THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'end_date',
    old_data = OLD.end_date,
    new_data = NEW.end_date,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.start_time IS NULL AND NEW.start_time IS NOT NULL) OR 
     (OLD.start_time IS NOT NULL AND NEW.start_time IS NULL) OR 
     NEW.start_time != OLD.start_time THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'start_time',
    old_data = OLD.start_time,
    new_data = NEW.start_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.end_time IS NULL AND NEW.end_time IS NOT NULL) OR 
     (OLD.end_time IS NOT NULL AND NEW.end_time IS NULL) OR 
     NEW.end_time != OLD.end_time THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'end_time',
    old_data = OLD.end_time,
    new_data = NEW.end_time,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.car_type IS NULL AND NEW.car_type IS NOT NULL) OR 
     (OLD.car_type IS NOT NULL AND NEW.car_type IS NULL) OR 
     NEW.car_type != OLD.car_type THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'car_type',
    old_data = OLD.car_type,
    new_data = NEW.car_type,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.car_number IS NULL AND NEW.car_number IS NOT NULL) OR 
     (OLD.car_number IS NOT NULL AND NEW.car_number IS NULL) OR 
     NEW.car_number != OLD.car_number THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'car_number',
    old_data = OLD.car_number,
    new_data = NEW.car_number,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.unknown_car IS NULL AND NEW.unknown_car IS NOT NULL) OR 
     (OLD.unknown_car IS NOT NULL AND NEW.unknown_car IS NULL) OR 
     NEW.unknown_car != OLD.unknown_car THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'unknown_car',
    old_data = OLD.unknown_car,
    new_data = NEW.unknown_car,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.price IS NULL AND NEW.price IS NOT NULL) OR 
     (OLD.price IS NOT NULL AND NEW.price IS NULL) OR 
     NEW.price != OLD.price THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'price',
    old_data = OLD.price,
    new_data = NEW.price,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.price_owner IS NULL AND NEW.price_owner IS NOT NULL) OR 
     (OLD.price_owner IS NOT NULL AND NEW.price_owner IS NULL) OR 
     NEW.price_owner != OLD.price_owner THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'price_owner',
    old_data = OLD.price_owner,
    new_data = NEW.price_owner,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.price_total IS NULL AND NEW.price_total IS NOT NULL) OR 
     (OLD.price_total IS NOT NULL AND NEW.price_total IS NULL) OR 
     NEW.price_total != OLD.price_total THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'price_total',
    old_data = OLD.price_total,
    new_data = NEW.price_total,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.price_owner_total IS NULL AND NEW.price_owner_total IS NOT NULL) OR 
     (OLD.price_owner_total IS NOT NULL AND NEW.price_owner_total IS NULL) OR 
     NEW.price_owner_total != OLD.price_owner_total THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'price_owner_total',
    old_data = OLD.price_owner_total,
    new_data = NEW.price_owner_total,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.pay_rate IS NULL AND NEW.pay_rate IS NOT NULL) OR 
     (OLD.pay_rate IS NOT NULL AND NEW.pay_rate IS NULL) OR 
     NEW.pay_rate != OLD.pay_rate THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'pay_rate',
    old_data = OLD.pay_rate,
    new_data = NEW.pay_rate,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.phone_number IS NULL AND NEW.phone_number IS NOT NULL) OR 
     (OLD.phone_number IS NOT NULL AND NEW.phone_number IS NULL) OR 
     NEW.phone_number != OLD.phone_number THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'phone_number',
    old_data = OLD.phone_number,
    new_data = NEW.phone_number,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.amount IS NULL AND NEW.amount IS NOT NULL) OR 
     (OLD.amount IS NOT NULL AND NEW.amount IS NULL) OR 
     NEW.amount != OLD.amount THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'amount',
    old_data = OLD.amount,
    new_data = NEW.amount,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.cancel_amount IS NULL AND NEW.cancel_amount IS NOT NULL) OR 
     (OLD.cancel_amount IS NOT NULL AND NEW.cancel_amount IS NULL) OR 
     NEW.cancel_amount != OLD.cancel_amount THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'cancel_amount',
    old_data = OLD.cancel_amount,
    new_data = NEW.cancel_amount,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.tax IS NULL AND NEW.tax IS NOT NULL) OR 
     (OLD.tax IS NOT NULL AND NEW.tax IS NULL) OR 
     NEW.tax != OLD.tax THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'tax',
    old_data = OLD.tax,
    new_data = NEW.tax,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.refunded_including_tax IS NULL AND NEW.refunded_including_tax IS NOT NULL) OR 
     (OLD.refunded_including_tax IS NOT NULL AND NEW.refunded_including_tax IS NULL) OR 
     NEW.refunded_including_tax != OLD.refunded_including_tax THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'refunded_including_tax',
    old_data = OLD.refunded_including_tax,
    new_data = NEW.refunded_including_tax,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.nosales IS NULL AND NEW.nosales IS NOT NULL) OR 
     (OLD.nosales IS NOT NULL AND NEW.nosales IS NULL) OR 
     NEW.nosales != OLD.nosales THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'nosales',
    old_data = OLD.nosales,
    new_data = NEW.nosales,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.nopayment IS NULL AND NEW.nopayment IS NOT NULL) OR 
     (OLD.nopayment IS NOT NULL AND NEW.nopayment IS NULL) OR 
     NEW.nopayment != OLD.nopayment THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'nopayment',
    old_data = OLD.nopayment,
    new_data = NEW.nopayment,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.nouse IS NULL AND NEW.nouse IS NOT NULL) OR 
     (OLD.nouse IS NOT NULL AND NEW.nouse IS NULL) OR 
     NEW.nouse != OLD.nouse THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'nouse',
    old_data = OLD.nouse,
    new_data = NEW.nouse,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.staff_comment IS NULL AND NEW.staff_comment IS NOT NULL) OR 
     (OLD.staff_comment IS NOT NULL AND NEW.staff_comment IS NULL) OR 
     NEW.staff_comment != OLD.staff_comment THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'staff_comment',
    old_data = OLD.staff_comment,
    new_data = NEW.staff_comment,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR 
     (OLD.status IS NOT NULL AND NEW.status IS NULL) OR 
     NEW.status != OLD.status THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'status',
    old_data = OLD.status,
    new_data = NEW.status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.payment_status IS NULL AND NEW.payment_status IS NOT NULL) OR 
     (OLD.payment_status IS NOT NULL AND NEW.payment_status IS NULL) OR 
     NEW.payment_status != OLD.payment_status THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'payment_status',
    old_data = OLD.payment_status,
    new_data = NEW.payment_status,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.coinparking IS NULL AND NEW.coinparking IS NOT NULL) OR 
     (OLD.coinparking IS NOT NULL AND NEW.coinparking IS NULL) OR 
     NEW.coinparking != OLD.coinparking THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'coinparking',
    old_data = OLD.coinparking,
    new_data = NEW.coinparking,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.refunded_point IS NULL AND NEW.refunded_point IS NOT NULL) OR 
     (OLD.refunded_point IS NOT NULL AND NEW.refunded_point IS NULL) OR 
     NEW.refunded_point != OLD.refunded_point THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'refunded_point',
    old_data = OLD.refunded_point,
    new_data = NEW.refunded_point,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

  IF (OLD.used_point IS NULL AND NEW.used_point IS NOT NULL) OR 
     (OLD.used_point IS NOT NULL AND NEW.used_point IS NULL) OR 
     NEW.used_point != OLD.used_point THEN
    INSERT INTO update_history SET
    uniq_id = NEW.reservation_id,
    table_name = 'reservation',
    column_name = 'used_point',
    old_data = OLD.used_point,
    new_data = NEW.used_point,
    update_user_id = NEW.update_user_id,
    created_at = NOW();
  END IF;

END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ reservation_changed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation_changed`;

CREATE TABLE `reservation_changed` (
  `reservation_changed_id` varchar(100) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `before_start_at` datetime NOT NULL,
  `before_end_at` datetime NOT NULL,
  `after_start_at` datetime NOT NULL,
  `after_end_at` datetime NOT NULL,
  `before_price_total` int(11) NOT NULL,
  `after_price_total` int(11) NOT NULL,
  `price_total` int(11) NOT NULL,
  `used_point` int(10) unsigned DEFAULT NULL,
  `refunded_point` int(10) unsigned DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `cancel_amount` int(10) unsigned DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `refunded_including_tax` int(11) DEFAULT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `gmo_order_id` varchar(27) DEFAULT NULL,
  `gmo_access_id` varchar(32) DEFAULT NULL,
  `gmo_access_password` varchar(32) DEFAULT NULL,
  `gmo_process_date` varchar(14) DEFAULT NULL,
  `trading_id` varchar(100) DEFAULT NULL,
  `payment_id` varchar(100) DEFAULT NULL,
  `gid` varchar(100) DEFAULT NULL,
  `paymentdata` text,
  `status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_career_type` int(11) DEFAULT NULL,
  `app_kind` varchar(10) DEFAULT NULL,
  `maximum_price_type` varchar(20) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  `reserve_time` datetime DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  `payment_notice_time` datetime DEFAULT NULL,
  `capture_time` datetime DEFAULT NULL,
  `timeout_time` datetime DEFAULT NULL,
  `notice_time` datetime DEFAULT NULL,
  `start_noticed_at` datetime DEFAULT NULL,
  `end_noticed_at` datetime DEFAULT NULL,
  `point_applied_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reservation_changed_id`),
  KEY `idx_trading_id` (`trading_id`),
  KEY `idx_reservationid_status` (`reservation_id`,`status`),
  KEY `idx_point_applied_at` (`point_applied_at`),
  KEY `idx_status_registtime` (`status`,`regist_time`),
  KEY `idx_paymenttype_paymentstatus_capturetime_gid` (`payment_type`,`payment_status`,`capture_time`,`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ reservation_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation_detail`;

CREATE TABLE `reservation_detail` (
  `reservation_detail_id` varchar(100) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `parking_space_id` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `price` int(11) DEFAULT NULL,
  `price_owner` int(11) DEFAULT NULL,
  `price_half` tinyint(1) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  `special_delete` tinyint(4) DEFAULT NULL,
  `external_reservation_detail_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`reservation_detail_id`),
  KEY `idx_parkingspace_date` (`parking_space_id`,`date`),
  KEY `idx_reservation_status_date` (`reservation_id`,`status`,`date`),
  KEY `idx_date_status` (`date`,`status`),
  KEY `idx_parkingid_status_date` (`parking_id`,`status`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ reservation_sublease
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation_sublease`;

CREATE TABLE `reservation_sublease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `prefecture` varchar(10) DEFAULT NULL,
  `year_month` int(11) NOT NULL,
  `sales_amount` int(11) NOT NULL,
  `cost_amount` int(11) NOT NULL,
  `del_flag` tinyint(1) DEFAULT '0',
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_company` (`company_name`,`shop_name`,`year_month`),
  KEY `idx_year_month_prefecture` (`year_month`,`prefecture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ reservation_thanks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation_thanks`;

CREATE TABLE `reservation_thanks` (
  `reservation_id` varchar(100) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ retention_rate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retention_rate`;

CREATE TABLE `retention_rate` (
  `retention_rate_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `first_reserved_month` varchar(6) NOT NULL DEFAULT '',
  `next_reserved_terms` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`retention_rate_id`),
  UNIQUE KEY `unique_next_reserved_user` (`user_id`,`next_reserved_terms`,`first_reserved_month`),
  KEY `idx_first_reserved_month` (`first_reserved_month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ rival_company_parking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rival_company_parking`;

CREATE TABLE `rival_company_parking` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_cd` varchar(100) NOT NULL DEFAULT '',
  `create_date` date DEFAULT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `space_count` int(11) unsigned DEFAULT NULL,
  `url` varchar(400) DEFAULT NULL,
  `price_raw_data` varchar(2000) DEFAULT NULL,
  `prefecture` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `weekday_info` varchar(100) DEFAULT NULL,
  `weekday_price` int(11) unsigned DEFAULT NULL,
  `weekday_max` int(11) unsigned DEFAULT NULL,
  `holiday_info` varchar(100) DEFAULT NULL,
  `holiday_price` int(11) unsigned DEFAULT NULL,
  `holiday_max` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_cd` (`company_cd`,`create_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ rival_company_parking_display
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rival_company_parking_display`;

CREATE TABLE `rival_company_parking_display` (
  `company_cd` varchar(100) NOT NULL DEFAULT '',
  `create_date` date NOT NULL,
  PRIMARY KEY (`company_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `date` date NOT NULL,
  `reservation_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `parking_id` varchar(100) DEFAULT NULL,
  `parking_name` varchar(200) DEFAULT NULL,
  `prefecture` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `rental_type` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `sort` varchar(10) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(10) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` varchar(10) DEFAULT NULL,
  `total_amount` int(11) NOT NULL DEFAULT '0',
  `coupon_amount` int(11) NOT NULL DEFAULT '0',
  `refunded_point` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `used_point` int(11) NOT NULL DEFAULT '0',
  `amount_include_tax` int(11) NOT NULL DEFAULT '0',
  `coupon_id` varchar(100) DEFAULT '',
  `coupon_code_id` varchar(100) DEFAULT NULL,
  `coupon_name` varchar(100) DEFAULT NULL,
  `coupon_type` varchar(20) DEFAULT NULL,
  `coupon_days` int(11) DEFAULT NULL,
  `charged_cancel` varchar(10) DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  `checked_out_at` datetime DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `capture_time` datetime DEFAULT NULL,
  `paygent_id` varchar(100) DEFAULT NULL,
  `cloudpayment_id` varchar(100) DEFAULT NULL,
  `gmo_id` varchar(100) DEFAULT NULL,
  `payment_career_type` varchar(20) DEFAULT NULL,
  `user_type` varchar(20) DEFAULT NULL,
  `price_owner_total` int(11) NOT NULL DEFAULT '0',
  `pay_rate` int(11) NOT NULL DEFAULT '0',
  `pay_amount` int(11) NOT NULL DEFAULT '0',
  `contract_type` varchar(20) DEFAULT NULL,
  `area_type` varchar(20) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `ps_release_time` datetime DEFAULT NULL,
  `is_exclude_sales` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `idx_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sales_irregular
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_irregular`;

CREATE TABLE `sales_irregular` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kind` varchar(20) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) DEFAULT NULL,
  `parking_id` varchar(100) NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `is_erasures` tinyint(4) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL,
  `user_payment_name` varchar(100) DEFAULT NULL,
  `user_billed_amount` int(11) NOT NULL DEFAULT '0',
  `user_start_date` date DEFAULT NULL,
  `user_end_date` date DEFAULT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `user_phone_number` varchar(20) DEFAULT NULL,
  `user_note` text,
  `deadline_date` date DEFAULT NULL,
  `owner_start_date` date DEFAULT NULL,
  `owner_end_date` date DEFAULT NULL,
  `owner_price` int(11) NOT NULL DEFAULT '0',
  `pay_rate` int(11) NOT NULL DEFAULT '0',
  `pay_amount` int(11) NOT NULL DEFAULT '0',
  `owner_note` text,
  `approval_date` date DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `receipt_amount` int(11) NOT NULL DEFAULT '0',
  `payment_name` varchar(100) DEFAULT NULL,
  `memo` text,
  `remind1_date` date DEFAULT NULL,
  `remind1_user_id` varchar(100) DEFAULT NULL,
  `remind1_deadline_date` date DEFAULT NULL,
  `remind2_date` date DEFAULT NULL,
  `remind2_user_id` varchar(100) DEFAULT NULL,
  `remind2_deadline_date` date DEFAULT NULL,
  `create_section` int(11) NOT NULL,
  `create_user_id` varchar(100) NOT NULL DEFAULT '',
  `update_user_id` varchar(100) NOT NULL DEFAULT '',
  `approve_user_id` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_approval_date` (`approval_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ scraping_parking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `scraping_parking`;

CREATE TABLE `scraping_parking` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_type` varchar(20) NOT NULL DEFAULT '',
  `url` varchar(1000) NOT NULL DEFAULT '',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ scraping_parking_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `scraping_parking_contents`;

CREATE TABLE `scraping_parking_contents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_type` varchar(20) NOT NULL DEFAULT '',
  `parking_id` varchar(100) NOT NULL DEFAULT '',
  `scraping_started_at` datetime NOT NULL,
  `vacancy` varchar(20) NOT NULL DEFAULT '',
  `note` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ search_keyword
# ------------------------------------------------------------

DROP TABLE IF EXISTS `search_keyword`;

CREATE TABLE `search_keyword` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(100) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sharegate_password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sharegate_password`;

CREATE TABLE `sharegate_password` (
  `password_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `password` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`password_id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sharegate_password_relation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sharegate_password_relation`;

CREATE TABLE `sharegate_password_relation` (
  `reservation_id` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `password_id` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  KEY `idx_reservation_id` (`reservation_id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ spot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spot`;

CREATE TABLE `spot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spot_genre_id` int(11) NOT NULL,
  `code` varchar(31) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `prefecture_id` int(11) NOT NULL,
  `classification` int(11) NOT NULL DEFAULT '1',
  `kana` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `context` text,
  `custom_section` text,
  `enabled_linked_spots` tinyint(1) NOT NULL DEFAULT '0',
  `linked_spots_title` varchar(255) DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `market_price` int(11) NOT NULL DEFAULT '0',
  `unit_market_price` int(11) NOT NULL DEFAULT '0',
  `area_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `radius` int(11) NOT NULL DEFAULT '1000',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ spot_daily_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spot_daily_total`;

CREATE TABLE `spot_daily_total` (
  `date` date NOT NULL,
  `spot_key` varchar(200) NOT NULL,
  `used_count` int(11) NOT NULL DEFAULT '0',
  `active_space_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`date`,`spot_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ spot_genre
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spot_genre`;

CREATE TABLE `spot_genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ spot_linked_spots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spot_linked_spots`;

CREATE TABLE `spot_linked_spots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_spot_id` int(11) NOT NULL,
  `spot_id` int(11) NOT NULL,
  `distance` int(11) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_spot_relation` (`parent_spot_id`,`spot_id`),
  KEY `parent_spot_id` (`parent_spot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ spot_parkings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spot_parkings`;

CREATE TABLE `spot_parkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_id` varchar(100) NOT NULL,
  `spot_id` int(11) NOT NULL,
  `contains` tinyint(1) NOT NULL DEFAULT '1',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `favorite` int(11) NOT NULL DEFAULT '0',
  `distance` int(11) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL DEFAULT '0',
  `travel_mode` tinyint(1) NOT NULL DEFAULT '0',
  `discard` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_spot_parking` (`parking_id`,`spot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `facebook_user_id` varchar(100) NOT NULL DEFAULT '',
  `facebook_username` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `role` varchar(20) NOT NULL DEFAULT '',
  `status` varchar(20) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ support_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `support_history`;

CREATE TABLE `support_history` (
  `support_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `comment` text CHARACTER SET utf8,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`support_id`),
  KEY `idx_userid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ tracking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tracking`;

CREATE TABLE `tracking` (
  `tracking_id` varchar(100) DEFAULT '',
  `uri` varchar(400) NOT NULL DEFAULT '',
  `user_agent` varchar(400) NOT NULL DEFAULT '',
  `message` varchar(400) DEFAULT NULL,
  `regist_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ uaid_storage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `uaid_storage`;

CREATE TABLE `uaid_storage` (
  `uaid` varchar(100) NOT NULL DEFAULT '',
  `data` text,
  PRIMARY KEY (`uaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ update_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `update_history`;

CREATE TABLE `update_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uniq_id` varchar(100) NOT NULL DEFAULT '',
  `table_name` varchar(40) NOT NULL DEFAULT '',
  `column_name` varchar(40) NOT NULL DEFAULT '',
  `old_data` text,
  `new_data` text,
  `update_user_id` varchar(100) DEFAULT '',
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_update_history_uniq_id` (`uniq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `parent_user_id` varchar(100) DEFAULT NULL,
  `invite_id` varchar(20) DEFAULT NULL,
  `use_invite_id` varchar(20) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT '',
  `phone_number` varchar(20) DEFAULT NULL,
  `phone_reachable_time` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `facebook_user_id` varchar(100) DEFAULT NULL,
  `google_user_id` varchar(100) DEFAULT NULL,
  `au_open_id` varchar(200) DEFAULT NULL,
  `line_id` varchar(100) DEFAULT NULL,
  `melmaga` varchar(20) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `car_type` varchar(100) DEFAULT NULL,
  `car_number` varchar(100) DEFAULT NULL,
  `car_model` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `bank_type` varchar(20) DEFAULT NULL COMMENT 'y: ゆうちょ銀行, b: その他の銀行',
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_branch` varchar(100) DEFAULT NULL COMMENT 'ゆうちょ銀行の場合は記号',
  `bank_account_number` varchar(100) DEFAULT NULL,
  `bank_account_lname` varchar(100) DEFAULT NULL,
  `bank_account_fname` varchar(100) DEFAULT NULL,
  `yucho_code` varchar(100) DEFAULT NULL,
  `yucho_account_number` varchar(100) DEFAULT NULL,
  `yucho_account_lname` varchar(100) DEFAULT NULL,
  `yucho_account_fname` varchar(100) DEFAULT NULL,
  `staff_comment` text,
  `status` varchar(20) NOT NULL DEFAULT '',
  `email_status` varchar(20) NOT NULL DEFAULT '',
  `owner` int(11) DEFAULT NULL,
  `owner_campaign_id` varchar(100) DEFAULT NULL,
  `corp_payment_key` varchar(100) DEFAULT NULL,
  `corp_payment_method` tinyint(1) DEFAULT NULL,
  `preregist` int(11) DEFAULT NULL,
  `reservation_badge` int(11) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  `activate_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `lockout_time` datetime DEFAULT NULL,
  `leave_time` datetime DEFAULT NULL,
  `reservation_badge_time` datetime DEFAULT NULL,
  `reservation_alert_time` datetime DEFAULT NULL,
  `is_unpaid` tinyint(1) DEFAULT NULL,
  `notify_anytime` tinyint(1) NOT NULL DEFAULT '1',
  `notify_only_reservation_of_current_day` tinyint(1) NOT NULL DEFAULT '0',
  `notice_ways` tinyint(1) NOT NULL DEFAULT '1',
  `update_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_facebook` (`facebook_user_id`),
  KEY `idx_email` (`email`),
  KEY `idx_google` (`google_user_id`),
  KEY `idx_family_id` (`user_id`,`parent_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `insert_user` AFTER INSERT ON `user` FOR EACH ROW BEGIN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'name',
new_data = NEW.name,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'phone_number',
new_data = NEW.phone_number,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'email',
new_data = NEW.email,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'password',
new_data = NEW.password,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'facebook_user_id',
new_data = NEW.facebook_user_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'google_user_id',
new_data = NEW.google_user_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'line_id',
new_data = NEW.line_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'status',
new_data = NEW.status,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'company',
new_data = NEW.company,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'melmaga',
new_data = NEW.melmaga,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_type',
new_data = NEW.car_type,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_number',
new_data = NEW.car_number,
update_user_id = NEW.update_user_id,
created_at = NOW();
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_model',
new_data = NEW.car_model,
update_user_id = NEW.update_user_id,
created_at = NOW();
END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`akippa`@`%` */ /*!50003 TRIGGER `update_user` AFTER UPDATE ON `user` FOR EACH ROW BEGIN
IF (OLD.name IS NULL AND NEW.name IS NOT NULL) OR 
(OLD.name IS NOT NULL AND NEW.name IS NULL) OR 
NEW.name != OLD.name THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'name',
old_data = OLD.name,
new_data = NEW.name,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.phone_number IS NULL AND NEW.phone_number IS NOT NULL) OR 
(OLD.phone_number IS NOT NULL AND NEW.phone_number IS NULL) OR 
NEW.phone_number != OLD.phone_number THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'phone_number',
old_data = OLD.phone_number,
new_data = NEW.phone_number,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.email IS NULL AND NEW.email IS NOT NULL) OR 
(OLD.email IS NOT NULL AND NEW.email IS NULL) OR 
NEW.email != OLD.email THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'email',
old_data = OLD.email,
new_data = NEW.email,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.password IS NULL AND NEW.password IS NOT NULL) OR 
(OLD.password IS NOT NULL AND NEW.password IS NULL) OR 
NEW.password != OLD.password THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'password',
old_data = OLD.password,
new_data = NEW.password,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.facebook_user_id IS NULL AND NEW.facebook_user_id IS NOT NULL) OR 
(OLD.facebook_user_id IS NOT NULL AND NEW.facebook_user_id IS NULL) OR 
NEW.facebook_user_id != OLD.facebook_user_id THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'facebook_user_id',
old_data = OLD.facebook_user_id,
new_data = NEW.facebook_user_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.google_user_id IS NULL AND NEW.google_user_id IS NOT NULL) OR 
(OLD.google_user_id IS NOT NULL AND NEW.google_user_id IS NULL) OR 
NEW.google_user_id != OLD.google_user_id THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'google_user_id',
old_data = OLD.google_user_id,
new_data = NEW.google_user_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.line_id IS NULL AND NEW.line_id IS NOT NULL) OR 
(OLD.line_id IS NOT NULL AND NEW.line_id IS NULL) OR 
NEW.line_id != OLD.line_id THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'line_id',
old_data = OLD.line_id,
new_data = NEW.line_id,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.status IS NULL AND NEW.status IS NOT NULL) OR 
(OLD.status IS NOT NULL AND NEW.status IS NULL) OR 
NEW.status != OLD.status THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'status',
old_data = OLD.status,
new_data = NEW.status,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.company IS NULL AND NEW.company IS NOT NULL) OR 
(OLD.company IS NOT NULL AND NEW.company IS NULL) OR 
NEW.company != OLD.company THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'company',
old_data = OLD.company,
new_data = NEW.company,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.melmaga IS NULL AND NEW.melmaga IS NOT NULL) OR 
(OLD.melmaga IS NOT NULL AND NEW.melmaga IS NULL) OR 
NEW.melmaga != OLD.melmaga THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'melmaga',
old_data = OLD.melmaga,
new_data = NEW.melmaga,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.car_type IS NULL AND NEW.car_type IS NOT NULL) OR 
(OLD.car_type IS NOT NULL AND NEW.car_type IS NULL) OR 
NEW.car_type != OLD.car_type THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_type',
old_data = OLD.car_type,
new_data = NEW.car_type,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.car_number IS NULL AND NEW.car_number IS NOT NULL) OR 
(OLD.car_number IS NOT NULL AND NEW.car_number IS NULL) OR 
NEW.car_number != OLD.car_number THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_number',
old_data = OLD.car_number,
new_data = NEW.car_number,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
IF (OLD.car_model IS NULL AND NEW.car_model IS NOT NULL) OR 
(OLD.car_model IS NOT NULL AND NEW.car_model IS NULL) OR 
NEW.car_model != OLD.car_model THEN
INSERT INTO update_history SET
uniq_id = NEW.user_id,
table_name = 'user',
column_name = 'car_model',
old_data = OLD.car_model,
new_data = NEW.car_model,
update_user_id = NEW.update_user_id,
created_at = NOW();
END IF;
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# テーブルのダンプ user_friend_invite_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_friend_invite_code`;

CREATE TABLE `user_friend_invite_code` (
  `user_id` varchar(100) NOT NULL,
  `user_friend_invite_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ user_friend_invite_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_friend_invite_history`;

CREATE TABLE `user_friend_invite_history` (
  `user_id` varchar(100) NOT NULL,
  `inviter_user_id` varchar(100) NOT NULL,
  `regist_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# テーブルのダンプ user_invite_id
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_invite_id`;

CREATE TABLE `user_invite_id` (
  `invite_id` varchar(100) NOT NULL DEFAULT '',
  `campaign_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`invite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_invited_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_invited_history`;

CREATE TABLE `user_invited_history` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `invite_id` varchar(100) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_organization`;

CREATE TABLE `user_organization` (
  `user_id` varchar(100) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `default_display` int(11) NOT NULL DEFAULT '1',
  `user_authority` tinyint(4) NOT NULL DEFAULT '0',
  `parking_authority` tinyint(4) NOT NULL DEFAULT '0',
  `payment_detail_authority` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_organization_payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_organization_payment`;

CREATE TABLE `user_organization_payment` (
  `user_id` varchar(100) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_point
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_point`;

CREATE TABLE `user_point` (
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `amount` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_point_expired
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_point_expired`;

CREATE TABLE `user_point_expired` (
  `user_point_expired_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `amount` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`user_point_expired_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_point_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_point_history`;

CREATE TABLE `user_point_history` (
  `user_point_history_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) DEFAULT NULL,
  `reservation_changed_id` varchar(100) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `point_delete_admin_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`user_point_history_id`),
  KEY `idx_userid_createdat` (`user_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_point_lock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_point_lock`;

CREATE TABLE `user_point_lock` (
  `user_point_lock_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `reservation_id` varchar(100) DEFAULT NULL,
  `reservation_changed_id` varchar(100) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`user_point_lock_id`),
  KEY `idx_userid` (`user_id`),
  KEY `idx_reservation_id` (`reservation_id`,`reservation_changed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_return_rate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_return_rate`;

CREATE TABLE `user_return_rate` (
  `user_id` varchar(100) NOT NULL,
  `first_reservation_date` date DEFAULT NULL,
  `second_reservation_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ user_validation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_validation`;

CREATE TABLE `user_validation` (
  `validation_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`validation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ utm_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `utm_info`;

CREATE TABLE `utm_info` (
  `utm_id` varchar(100) NOT NULL DEFAULT '',
  `target` varchar(20) NOT NULL DEFAULT '',
  `target_id` varchar(100) NOT NULL DEFAULT '',
  `utm_source` varchar(100) NOT NULL DEFAULT '',
  `utm_medium` varchar(100) DEFAULT '',
  `utm_term` varchar(100) DEFAULT NULL,
  `utm_content` varchar(100) DEFAULT NULL,
  `utm_campaign` varchar(100) DEFAULT NULL,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`utm_id`),
  KEY `idx_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ utm_title
# ------------------------------------------------------------

DROP TABLE IF EXISTS `utm_title`;

CREATE TABLE `utm_title` (
  `utm_source` varchar(100) NOT NULL DEFAULT '',
  `utm_medium` varchar(100) DEFAULT NULL,
  `utm_term` varchar(100) DEFAULT NULL,
  `utm_content` varchar(100) DEFAULT NULL,
  `utm_campaign` varchar(100) DEFAULT NULL,
  `title` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ v_daily_reservation_last_date
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_daily_reservation_last_date`;

CREATE TABLE `v_daily_reservation_last_date` (
   `reservation_id` VARCHAR(100) NOT NULL DEFAULT '',
   `last_date` DATE NULL DEFAULT NULL
) ENGINE=MyISAM;



# テーブルのダンプ v_notify_destination_all
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_notify_destination_all`;

CREATE TABLE `v_notify_destination_all` (
   `user_id` VARCHAR(100) NOT NULL DEFAULT '',
   `company` VARCHAR(100) NULL DEFAULT NULL,
   `name` VARCHAR(100) NULL DEFAULT NULL,
   `設定箇所` VARCHAR(6) NOT NULL DEFAULT '',
   `status` VARCHAR(20) NOT NULL DEFAULT '',
   `notify_anytime` TINYINT(4) NOT NULL DEFAULT '0',
   `送信時間制限` VARCHAR(19) NOT NULL DEFAULT '',
   `notify_only_reservation_of_current_day` TINYINT(4) NOT NULL DEFAULT '0',
   `予約通知` VARCHAR(6) NOT NULL DEFAULT '',
   `notice_ways` TINYINT(4) NOT NULL DEFAULT '0',
   `通知方法` VARCHAR(5) NOT NULL DEFAULT '',
   `送信先` VARCHAR(200) NULL DEFAULT NULL,
   `予約完了通知` INT(11) NOT NULL DEFAULT '0',
   `予約情報変更通知` INT(11) NOT NULL DEFAULT '0',
   `予約キャンセル通知` INT(11) NOT NULL DEFAULT '0',
   `時間貸し予約変更通知` INT(11) NOT NULL DEFAULT '0',
   `駐車場料金変更通知` INT(11) NOT NULL DEFAULT '0',
   `写真アップロード依頼通知` INT(11) NOT NULL DEFAULT '0',
   `駐車場掲載許可通知` INT(11) NOT NULL DEFAULT '0',
   `★当日予約一覧通知` INT(11) NOT NULL DEFAULT '0',
   `初めての予約通知` INT(11) NOT NULL DEFAULT '0',
   `regist_time` VARCHAR(19) NOT NULL DEFAULT ''
) ENGINE=MyISAM;



# テーブルのダンプ v_notify_destination_Individual
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_notify_destination_Individual`;

CREATE TABLE `v_notify_destination_Individual` (
   `user_id` VARCHAR(100) NOT NULL DEFAULT '',
   `company` VARCHAR(100) NULL DEFAULT NULL,
   `name` VARCHAR(100) NULL DEFAULT '',
   `設定箇所` VARCHAR(6) NOT NULL DEFAULT '',
   `status` VARCHAR(20) NOT NULL DEFAULT '',
   `notify_anytime` TINYINT(1) NOT NULL DEFAULT '1',
   `送信時間制限` VARCHAR(19) NOT NULL DEFAULT '',
   `notify_only_reservation_of_current_day` TINYINT(1) NOT NULL DEFAULT '0',
   `予約通知` VARCHAR(6) NOT NULL DEFAULT '',
   `notice_ways` TINYINT(1) NOT NULL DEFAULT '1',
   `通知方法` VARCHAR(5) NOT NULL DEFAULT '',
   `送信先` VARCHAR(200) NOT NULL DEFAULT '',
   `予約完了通知` INT(1) NOT NULL DEFAULT '0',
   `予約情報変更通知` INT(1) NOT NULL DEFAULT '0',
   `予約キャンセル通知` INT(1) NOT NULL DEFAULT '0',
   `時間貸し予約変更通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場料金変更通知` INT(1) NOT NULL DEFAULT '0',
   `写真アップロード依頼通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場掲載許可通知` INT(1) NOT NULL DEFAULT '0',
   `★当日予約一覧通知` INT(1) NOT NULL DEFAULT '0',
   `初めての予約通知` INT(1) NOT NULL DEFAULT '0',
   `regist_time` DATETIME NOT NULL
) ENGINE=MyISAM;



# テーブルのダンプ v_notify_destination_main_fax
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_notify_destination_main_fax`;

CREATE TABLE `v_notify_destination_main_fax` (
   `user_id` VARCHAR(100) NOT NULL DEFAULT '',
   `company` VARCHAR(100) NULL DEFAULT NULL,
   `name` VARCHAR(100) NULL DEFAULT '',
   `設定箇所` VARCHAR(6) NOT NULL DEFAULT '',
   `status` VARCHAR(20) NOT NULL DEFAULT '',
   `notify_anytime` TINYINT(1) NOT NULL DEFAULT '1',
   `送信時間制限` VARCHAR(19) NOT NULL DEFAULT '',
   `notify_only_reservation_of_current_day` TINYINT(1) NOT NULL DEFAULT '0',
   `予約通知` VARCHAR(6) NOT NULL DEFAULT '',
   `notice_ways` TINYINT(1) NOT NULL DEFAULT '1',
   `通知方法` VARCHAR(5) NOT NULL DEFAULT '',
   `送信先` VARCHAR(20) NULL DEFAULT NULL,
   `予約完了通知` INT(1) NOT NULL DEFAULT '0',
   `予約情報変更通知` INT(1) NOT NULL DEFAULT '0',
   `予約キャンセル通知` INT(1) NOT NULL DEFAULT '0',
   `時間貸し予約変更通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場料金変更通知` INT(1) NOT NULL DEFAULT '0',
   `写真アップロード依頼通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場掲載許可通知` INT(1) NOT NULL DEFAULT '0',
   `★当日予約一覧通知` INT(1) NOT NULL DEFAULT '0',
   `初めての予約通知` INT(1) NOT NULL DEFAULT '0',
   `regist_time` VARCHAR(1) NOT NULL DEFAULT ''
) ENGINE=MyISAM;



# テーブルのダンプ v_notify_destination_main_mail
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_notify_destination_main_mail`;

CREATE TABLE `v_notify_destination_main_mail` (
   `user_id` VARCHAR(100) NOT NULL DEFAULT '',
   `company` VARCHAR(100) NULL DEFAULT NULL,
   `name` VARCHAR(100) NULL DEFAULT '',
   `設定箇所` VARCHAR(6) NOT NULL DEFAULT '',
   `status` VARCHAR(20) NOT NULL DEFAULT '',
   `notify_anytime` TINYINT(1) NOT NULL DEFAULT '1',
   `送信時間制限` VARCHAR(19) NOT NULL DEFAULT '',
   `notify_only_reservation_of_current_day` TINYINT(1) NOT NULL DEFAULT '0',
   `予約通知` VARCHAR(6) NOT NULL DEFAULT '',
   `notice_ways` TINYINT(1) NOT NULL DEFAULT '1',
   `通知方法` VARCHAR(5) NOT NULL DEFAULT '',
   `送信先` VARCHAR(200) NULL DEFAULT NULL,
   `予約完了通知` INT(1) NOT NULL DEFAULT '0',
   `予約情報変更通知` INT(1) NOT NULL DEFAULT '0',
   `予約キャンセル通知` INT(1) NOT NULL DEFAULT '0',
   `時間貸し予約変更通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場料金変更通知` INT(1) NOT NULL DEFAULT '0',
   `写真アップロード依頼通知` INT(1) NOT NULL DEFAULT '0',
   `駐車場掲載許可通知` INT(1) NOT NULL DEFAULT '0',
   `★当日予約一覧通知` INT(1) NOT NULL DEFAULT '0',
   `初めての予約通知` INT(1) NOT NULL DEFAULT '0',
   `regist_time` VARCHAR(1) NOT NULL DEFAULT ''
) ENGINE=MyISAM;



# テーブルのダンプ v_point
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_point`;

CREATE TABLE `v_point` (
   `reservation_id` VARCHAR(100) NOT NULL DEFAULT '',
   `used_point` INT(11) UNSIGNED NULL DEFAULT NULL,
   `refunded_point` INT(11) UNSIGNED NULL DEFAULT NULL,
   `point_applied_at` DATETIME NULL DEFAULT NULL,
   `last_date` DATE NULL DEFAULT NULL
) ENGINE=MyISAM;



# テーブルのダンプ v_sales
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_sales`;

CREATE TABLE `v_sales` (
   `reservation_id` VARCHAR(100) NOT NULL DEFAULT '',
   `parking_id` VARCHAR(100) NOT NULL DEFAULT '',
   `parking_space_id` VARCHAR(100) NOT NULL DEFAULT '',
   `user_id` VARCHAR(100) NOT NULL DEFAULT '',
   `use_date` DATE NULL DEFAULT NULL,
   `price` INT(11) NULL DEFAULT NULL,
   `price_owner` INT(11) NULL DEFAULT NULL,
   `pay_rate` INT(11) NOT NULL DEFAULT '0',
   `pay_amount` INT(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# テーブルのダンプ v_sales_jp
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_sales_jp`;

CREATE TABLE `v_sales_jp` (
   `予約ID` VARCHAR(100) NOT NULL DEFAULT '',
   `利用日` DATE NULL DEFAULT NULL,
   `売上` INT(11) NULL DEFAULT NULL,
   `オーナー契約料金` INT(11) NULL DEFAULT NULL,
   `報酬率` INT(11) NOT NULL DEFAULT '0',
   `報酬額` INT(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# テーブルのダンプ violation_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `violation_record`;

CREATE TABLE `violation_record` (
  `violation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `comment` text,
  `regist_time` datetime NOT NULL,
  PRIMARY KEY (`violation_id`),
  KEY `idx_userid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ whats_new
# ------------------------------------------------------------

DROP TABLE IF EXISTS `whats_new`;

CREATE TABLE `whats_new` (
  `whats_new_id` varchar(100) NOT NULL DEFAULT '',
  `admin_id` varchar(100) NOT NULL DEFAULT '',
  `contents` text NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `regist_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`whats_new_id`),
  KEY `idx_regist_time` (`regist_time`),
  KEY `idx_status_regist_time` (`status`,`regist_time`),
  KEY `idx_type_status_regist_time` (`type`,`status`,`regist_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





# Replace placeholder table for v_sales_jp with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_sales_jp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_sales_jp`
AS SELECT
   `v_sales`.`reservation_id` AS `予約ID`,
   `v_sales`.`use_date` AS `利用日`,
   `v_sales`.`price` AS `売上`,
   `v_sales`.`price_owner` AS `オーナー契約料金`,
   `v_sales`.`pay_rate` AS `報酬率`,
   `v_sales`.`pay_amount` AS `報酬額`
FROM `v_sales`;


# Replace placeholder table for v_sales with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_sales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_sales`
AS SELECT
   `r`.`reservation_id` AS `reservation_id`,
   `r`.`parking_id` AS `parking_id`,
   `r`.`parking_space_id` AS `parking_space_id`,
   `r`.`user_id` AS `user_id`,
   `r`.`last_date` AS `use_date`,
   `r`.`price_total` AS `price`,
   `r`.`price_owner_total` AS `price_owner`,
   `s`.`pay_rate` AS `pay_rate`,
   `s`.`pay_amount` AS `pay_amount`
FROM (`reservation` `r` join `sales` `s` on(((`r`.`reservation_id` = `s`.`reservation_id`) and (`s`.`sort` = '1-main'))));


# Replace placeholder table for v_notify_destination_all with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_notify_destination_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_notify_destination_all`
AS SELECT
   `v_notify_destination_main_mail`.`user_id` AS `user_id`,
   `v_notify_destination_main_mail`.`company` AS `company`,
   `v_notify_destination_main_mail`.`name` AS `name`,
   `v_notify_destination_main_mail`.`設定箇所` AS `設定箇所`,
   `v_notify_destination_main_mail`.`status` AS `status`,
   `v_notify_destination_main_mail`.`notify_anytime` AS `notify_anytime`,
   `v_notify_destination_main_mail`.`送信時間制限` AS `送信時間制限`,
   `v_notify_destination_main_mail`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,
   `v_notify_destination_main_mail`.`予約通知` AS `予約通知`,
   `v_notify_destination_main_mail`.`notice_ways` AS `notice_ways`,
   `v_notify_destination_main_mail`.`通知方法` AS `通知方法`,
   `v_notify_destination_main_mail`.`送信先` AS `送信先`,
   `v_notify_destination_main_mail`.`予約完了通知` AS `予約完了通知`,
   `v_notify_destination_main_mail`.`予約情報変更通知` AS `予約情報変更通知`,
   `v_notify_destination_main_mail`.`予約キャンセル通知` AS `予約キャンセル通知`,
   `v_notify_destination_main_mail`.`時間貸し予約変更通知` AS `時間貸し予約変更通知`,
   `v_notify_destination_main_mail`.`駐車場料金変更通知` AS `駐車場料金変更通知`,
   `v_notify_destination_main_mail`.`写真アップロード依頼通知` AS `写真アップロード依頼通知`,
   `v_notify_destination_main_mail`.`駐車場掲載許可通知` AS `駐車場掲載許可通知`,
   `v_notify_destination_main_mail`.`★当日予約一覧通知` AS `★当日予約一覧通知`,
   `v_notify_destination_main_mail`.`初めての予約通知` AS `初めての予約通知`,
   `v_notify_destination_main_mail`.`regist_time` AS `regist_time`
FROM `v_notify_destination_main_mail` union all select `v_notify_destination_main_fax`.`user_id` AS `user_id`,`v_notify_destination_main_fax`.`company` AS `company`,`v_notify_destination_main_fax`.`name` AS `name`,`v_notify_destination_main_fax`.`設定箇所` AS `設定箇所`,`v_notify_destination_main_fax`.`status` AS `status`,`v_notify_destination_main_fax`.`notify_anytime` AS `notify_anytime`,`v_notify_destination_main_fax`.`送信時間制限` AS `送信時間制限`,`v_notify_destination_main_fax`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,`v_notify_destination_main_fax`.`予約通知` AS `予約通知`,`v_notify_destination_main_fax`.`notice_ways` AS `notice_ways`,`v_notify_destination_main_fax`.`通知方法` AS `通知方法`,`v_notify_destination_main_fax`.`送信先` AS `送信先`,`v_notify_destination_main_fax`.`予約完了通知` AS `予約完了通知`,`v_notify_destination_main_fax`.`予約情報変更通知` AS `予約情報変更通知`,`v_notify_destination_main_fax`.`予約キャンセル通知` AS `予約キャンセル通知`,`v_notify_destination_main_fax`.`時間貸し予約変更通知` AS `時間貸し予約変更通知`,`v_notify_destination_main_fax`.`駐車場料金変更通知` AS `駐車場料金変更通知`,`v_notify_destination_main_fax`.`写真アップロード依頼通知` AS `写真アップロード依頼通知`,`v_notify_destination_main_fax`.`駐車場掲載許可通知` AS `駐車場掲載許可通知`,`v_notify_destination_main_fax`.`★当日予約一覧通知` AS `★当日予約一覧通知`,`v_notify_destination_main_fax`.`初めての予約通知` AS `初めての予約通知`,`v_notify_destination_main_fax`.`regist_time` AS `regist_time` from `v_notify_destination_main_fax` union all select `v_notify_destination_Individual`.`user_id` AS `user_id`,`v_notify_destination_Individual`.`company` AS `company`,`v_notify_destination_Individual`.`name` AS `name`,`v_notify_destination_Individual`.`設定箇所` AS `設定箇所`,`v_notify_destination_Individual`.`status` AS `status`,`v_notify_destination_Individual`.`notify_anytime` AS `notify_anytime`,`v_notify_destination_Individual`.`送信時間制限` AS `送信時間制限`,`v_notify_destination_Individual`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,`v_notify_destination_Individual`.`予約通知` AS `予約通知`,`v_notify_destination_Individual`.`notice_ways` AS `notice_ways`,`v_notify_destination_Individual`.`通知方法` AS `通知方法`,`v_notify_destination_Individual`.`送信先` AS `送信先`,`v_notify_destination_Individual`.`予約完了通知` AS `予約完了通知`,`v_notify_destination_Individual`.`予約情報変更通知` AS `予約情報変更通知`,`v_notify_destination_Individual`.`予約キャンセル通知` AS `予約キャンセル通知`,`v_notify_destination_Individual`.`時間貸し予約変更通知` AS `時間貸し予約変更通知`,`v_notify_destination_Individual`.`駐車場料金変更通知` AS `駐車場料金変更通知`,`v_notify_destination_Individual`.`写真アップロード依頼通知` AS `写真アップロード依頼通知`,`v_notify_destination_Individual`.`駐車場掲載許可通知` AS `駐車場掲載許可通知`,`v_notify_destination_Individual`.`★当日予約一覧通知` AS `★当日予約一覧通知`,`v_notify_destination_Individual`.`初めての予約通知` AS `初めての予約通知`,`v_notify_destination_Individual`.`regist_time` AS `regist_time` from `v_notify_destination_Individual`;


# Replace placeholder table for v_notify_destination_main_mail with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_notify_destination_main_mail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_notify_destination_main_mail`
AS SELECT
   `user`.`user_id` AS `user_id`,
   `user`.`company` AS `company`,
   `user`.`name` AS `name`,'1.基本設定' AS `設定箇所`,
   `user`.`status` AS `status`,
   `user`.`notify_anytime` AS `notify_anytime`,(case when (`user`.`notify_anytime` = 0) then '08:00 - 20:59の間のみ送信' when (`user`.`notify_anytime` = 1) then '制限なし' else '例外' end) AS `送信時間制限`,
   `user`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,(case when (`user`.`notify_only_reservation_of_current_day` = 0) then '全予約' when (`user`.`notify_only_reservation_of_current_day` = 1) then '当日予約のみ' else '例外' end) AS `予約通知`,
   `user`.`notice_ways` AS `notice_ways`,'1.メール' AS `通知方法`,
   `user`.`email` AS `送信先`,1 AS `予約完了通知`,1 AS `予約情報変更通知`,1 AS `予約キャンセル通知`,1 AS `時間貸し予約変更通知`,1 AS `駐車場料金変更通知`,1 AS `写真アップロード依頼通知`,1 AS `駐車場掲載許可通知`,0 AS `★当日予約一覧通知`,1 AS `初めての予約通知`,'-' AS `regist_time`
FROM `user` where (((`user`.`notice_ways` = 1) or (`user`.`notice_ways` = 3)) and (`user`.`owner` = 1));


# Replace placeholder table for v_notify_destination_Individual with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_notify_destination_Individual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_notify_destination_Individual`
AS SELECT
   `u`.`user_id` AS `user_id`,
   `u`.`company` AS `company`,
   `u`.`name` AS `name`,'2.個別設定' AS `設定箇所`,
   `u`.`status` AS `status`,
   `u`.`notify_anytime` AS `notify_anytime`,(case when (`u`.`notify_anytime` = 0) then '08:00 - 20:59の間のみ送信' when (`u`.`notify_anytime` = 1) then '制限なし' else '例外' end) AS `送信時間制限`,
   `u`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,(case when (`u`.`notify_only_reservation_of_current_day` = 0) then '全予約' when (`u`.`notify_only_reservation_of_current_day` = 1) then '当日予約のみ' else '例外' end) AS `予約通知`,
   `u`.`notice_ways` AS `notice_ways`,(case when (`nd`.`send_type` = 1) then '1.メール' when (`nd`.`send_type` = 2) then '2.FAX' else '例外' end) AS `通知方法`,
   `nd`.`to` AS `送信先`,((`nd`.`notification_flag_to_owner` & 1) = 1) AS `予約完了通知`,((`nd`.`notification_flag_to_owner` & 2) = 2) AS `予約情報変更通知`,((`nd`.`notification_flag_to_owner` & 4) = 4) AS `予約キャンセル通知`,((`nd`.`notification_flag_to_owner` & 8) = 8) AS `時間貸し予約変更通知`,((`nd`.`notification_flag_to_owner` & 16) = 16) AS `駐車場料金変更通知`,((`nd`.`notification_flag_to_owner` & 32) = 32) AS `写真アップロード依頼通知`,((`nd`.`notification_flag_to_owner` & 64) = 64) AS `駐車場掲載許可通知`,((`nd`.`notification_flag_to_owner` & 128) = 128) AS `★当日予約一覧通知`,((`nd`.`notification_flag_to_owner` & 256) = 256) AS `初めての予約通知`,
   `nd`.`regist_time` AS `regist_time`
FROM (`notify_destination` `nd` join `user` `u`) where (`u`.`user_id` = `nd`.`user_id`);


# Replace placeholder table for v_notify_destination_main_fax with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_notify_destination_main_fax`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_notify_destination_main_fax`
AS SELECT
   `user`.`user_id` AS `user_id`,
   `user`.`company` AS `company`,
   `user`.`name` AS `name`,'1.基本設定' AS `設定箇所`,
   `user`.`status` AS `status`,
   `user`.`notify_anytime` AS `notify_anytime`,(case when (`user`.`notify_anytime` = 0) then '08:00 - 20:59の間のみ送信' when (`user`.`notify_anytime` = 1) then '制限なし' else '例外' end) AS `送信時間制限`,
   `user`.`notify_only_reservation_of_current_day` AS `notify_only_reservation_of_current_day`,(case when (`user`.`notify_only_reservation_of_current_day` = 0) then '全予約' when (`user`.`notify_only_reservation_of_current_day` = 1) then '当日予約のみ' else '例外' end) AS `予約通知`,
   `user`.`notice_ways` AS `notice_ways`,'2.FAX' AS `通知方法`,
   `user`.`fax` AS `送信先`,1 AS `予約完了通知`,1 AS `予約情報変更通知`,1 AS `予約キャンセル通知`,1 AS `時間貸し予約変更通知`,1 AS `駐車場料金変更通知`,1 AS `写真アップロード依頼通知`,1 AS `駐車場掲載許可通知`,0 AS `★当日予約一覧通知`,1 AS `初めての予約通知`,'-' AS `regist_time`
FROM `user` where (((`user`.`notice_ways` = 2) or (`user`.`notice_ways` = 3)) and (`user`.`owner` = 1));


# Replace placeholder table for v_daily_reservation_last_date with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_daily_reservation_last_date`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_daily_reservation_last_date`
AS SELECT
   `reservation_detail`.`reservation_id` AS `reservation_id`,max(`reservation_detail`.`date`) AS `last_date`
FROM `reservation_detail` where ((`reservation_detail`.`status` = 'reserved') or (`reservation_detail`.`date` = cast(`reservation_detail`.`cancel_time` as date))) group by `reservation_detail`.`reservation_id`;


# Replace placeholder table for v_point with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_point`;

CREATE ALGORITHM=UNDEFINED DEFINER=`akippa`@`%` SQL SECURITY DEFINER VIEW `v_point`
AS SELECT
   `r`.`reservation_id` AS `reservation_id`,
   `r`.`used_point` AS `used_point`,
   `r`.`refunded_point` AS `refunded_point`,
   `r`.`point_applied_at` AS `point_applied_at`,
   `r`.`last_date` AS `last_date`
FROM `reservation` `r` where ((`r`.`point_applied_at` is not null) and ((`r`.`status` = 'reserved') or ((`r`.`payment_status` <> 'canceled') and (`r`.`payment_status` <> 'error'))) and ((`r`.`used_point` > 0) or (`r`.`refunded_point` > 0)) and isnull(`r`.`special_delete`)) union all select `rc`.`reservation_id` AS `reservation_id`,`rc`.`used_point` AS `used_point`,`rc`.`refunded_point` AS `refunded_point`,`rc`.`point_applied_at` AS `point_applied_at`,`r`.`last_date` AS `last_date` from (`reservation` `r` join `reservation_changed` `rc` on((`r`.`reservation_id` = `rc`.`reservation_id`))) where ((`rc`.`point_applied_at` is not null) and ((`rc`.`status` = 'reserved') or ((`rc`.`payment_status` <> 'canceled') and (`rc`.`payment_status` <> 'error'))) and ((`rc`.`used_point` > 0) or (`rc`.`refunded_point` > 0)) and isnull(`r`.`special_delete`));

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
